package com.sxt.flutter.util;

import android.app.ActivityManager;
import android.content.Context;

import java.util.ArrayList;

public final class AppHelper {

    /**
     * 判断一个服务是否是开启状态
     *
     * @param serviceName 服务名
     * @return true -开启 false -关闭
     */
    public static boolean isServiceRunning(String serviceName,Context mCtx) {

        ActivityManager myManager = (ActivityManager) mCtx
                .getSystemService(Context.ACTIVITY_SERVICE);
        ArrayList<ActivityManager.RunningServiceInfo> runningService = null;
        if (myManager != null) {
            runningService = (ArrayList<ActivityManager.RunningServiceInfo>) myManager
                    .getRunningServices(Integer.MAX_VALUE);
        }
        if (runningService != null) {
            for (int i = 0; i < runningService.size(); i++) {
                String serviceName1 = runningService.get(i).service.getClassName().toString();
                if (serviceName1
                        .equals(serviceName)) {
                    return true;
                }
            }
        }
        return false;
    }

}
