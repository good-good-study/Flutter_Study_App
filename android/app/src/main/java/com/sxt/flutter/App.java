package com.sxt.flutter;

import android.content.Context;

import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;

import io.flutter.app.FlutterApplication;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class App extends FlutterApplication implements PluginRegistry.PluginRegistrantCallback {

  private static Context context;
  public static Context getCtx() {
    return context;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    context=this;

    StringBuffer param = new StringBuffer();
    param.append("appid="+getString(R.string.app_id)).append(",");
    // 设置使用v5+
    param.append(SpeechConstant.ENGINE_MODE+"="+ SpeechConstant.MODE_MSC);
    SpeechUtility.createUtility(this, param.toString());
  }

  @Override
  public void registerWith(PluginRegistry registry) {
    GeneratedPluginRegistrant.registerWith(registry);
  }
}