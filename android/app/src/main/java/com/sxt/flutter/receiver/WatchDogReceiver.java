package com.sxt.flutter.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.sxt.flutter.App;
import com.sxt.flutter.alarm.MainService;

public class WatchDogReceiver extends BroadcastReceiver {

    private final  String TAG ="WatchDogReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if(Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())){//开启完成
            Log.e(TAG,"开机完成");
            bootCompleted(context);
        }else if(Intent.ACTION_LOCKED_BOOT_COMPLETED.equals(intent.getAction())){
            Log.e(TAG,"开机完成，未解锁");
            bootCompleted(context);
        }else if(Intent.ACTION_USER_UNLOCKED.equals(intent.getAction())){
            Log.e(TAG,"开机完成，已解锁");
            bootCompleted(context);
        } else if (Intent.ACTION_SCREEN_OFF.equals(intent.getAction())) {
            Log.i(TAG, "锁屏了");
            bootCompleted(context);
        } else if (Intent.ACTION_USER_PRESENT.equals(intent.getAction())) {
            Log.i(TAG, "解锁了");
            bootCompleted(context);
        }else{
            //TODO
        }

    }

    /**
     * 开机完成，启动守护进程
     */
    private void bootCompleted(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(App.getCtx(), MainService.class));
        } else {
            context.startService(new Intent(App.getCtx(), MainService.class));
        }
    }
}
