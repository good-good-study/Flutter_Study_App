package com.sxt.flutter.alarm;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.sxt.flutter.App;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MyJobService extends JobService {

    final String TAG="MyJobService";

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.e(TAG,"onStartJob");
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.e(TAG,"onStopJob");
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForegroundService(new Intent(App.getCtx(), MainService.class));
//        } else {
            startService(new Intent(App.getCtx(), MainService.class));
//        }
        return false;
    }
}
