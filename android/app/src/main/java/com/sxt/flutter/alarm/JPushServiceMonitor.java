package com.sxt.flutter.alarm;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.sxt.flutter.App;
import com.sxt.flutter.util.AppHelper;

public class JPushServiceMonitor extends Worker {

    private final String TAG="Worker";

    public JPushServiceMonitor(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        boolean isPushServiceRunning = AppHelper.isServiceRunning("cn.jpush.android.service.PushService", App.getCtx());
        Log.e(TAG, "检测PushService是否存活");
        if (!isPushServiceRunning) {
            Log.e(TAG, "PushService挂掉了，重新启动");
            Context context = App.getCtx();
            Intent intent = new Intent(context, Uri.parse("cn.jpush.android.service.PushService").getClass());
            context.startService(intent);
        }

        return Result.success();
    }
}
