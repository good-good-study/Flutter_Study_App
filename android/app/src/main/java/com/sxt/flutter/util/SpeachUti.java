package com.sxt.flutter.util;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SynthesizerListener;
import com.iflytek.cloud.util.ResourceUtil;
import com.sxt.flutter.R;

public class SpeachUti {
    private static String TAG = "Audio";
    private Context context;
    private  SpeechSynthesizer mTts;// 语音合成对象
    private String voicerCloud="xiaoyan";// 默认云端发音人
    private  String voicerLocal="xiaoyan";// 默认本地发音人
    private int mPercentForBuffering = 0;//缓冲进度
    private int mPercentForPlaying = 0;//播放进度
    private String mEngineType = SpeechConstant.TYPE_CLOUD; //引擎类型
    private SpeachUti(){}

    public SpeachUti(Context context){
        this.context=context;
        init();
    }

    /**
     * 初始化合成对象
     */
    private void init(){
       if(mTts==null){
           mTts = SpeechSynthesizer.createSynthesizer(context, mTtsInitListener);
       }
    }

    /**
     * 开始合成
     * @param text 要合成语音的文本
     * @param engineType 引擎类型 （在线/本地）
     */
    public void startSpeaching(String text,String engineType){
        if(TextUtils.isEmpty(text))return;
        if(TextUtils.isEmpty(engineType))return;
        this.mEngineType=engineType;
        init();
        setParam();
        int code = mTts.startSpeaking(text, mTtsListener);
//			/**
//			 * 只保存音频不进行播放接口,调用此接口请注释startSpeaking接口
//			 * text:要合成的文本，uri:需要保存的音频全路径，listener:回调接口
//			*/
//			String path = Environment.getExternalStorageDirectory()+"/tts.pcm";
//			int code = mTts.synthesizeToUri(text, path, mTtsListener);

        if (code != ErrorCode.SUCCESS) {
            Log.e(TAG,String.format("语音合成失败,错误码:  %s",code));
        }
    }

    /**
     * 初始化监听。
     */
    private  InitListener mTtsInitListener = code -> {
        Log.d(TAG, "InitListener init() code = " + code);
        if (code != ErrorCode.SUCCESS) {
           Log.e(TAG,String.format("初始化失败,错误码：%s",+code));
        } else {
            // 初始化成功，之后可以调用startSpeaking方法
            // 注：有的开发者在onCreate方法中创建完合成对象之后马上就调用startSpeaking进行合成，
            // 正确的做法是将onCreate中的startSpeaking调用移至这里
        }
    };

    /**
     * 合成回调监听。
     */
    private SynthesizerListener mTtsListener = new SynthesizerListener() {

        @Override
        public void onSpeakBegin() {
            Log.e(TAG,"开始播放");
        }

        @Override
        public void onSpeakPaused() {
            Log.e(TAG,"暂停播放");
        }

        @Override
        public void onSpeakResumed() {
            Log.e(TAG,"继续播放");
        }

        @Override
        public void onBufferProgress(int percent, int beginPos, int endPos,
                                     String info) {
            // 合成进度
            mPercentForBuffering = percent;
           Log.e(TAG,String.format(context.getString(R.string.tts_toast_format),
                   mPercentForBuffering, mPercentForPlaying));
        }

        @Override
        public void onSpeakProgress(int percent, int beginPos, int endPos) {
            // 播放进度
            mPercentForPlaying = percent;
            Log.e(TAG,String.format(context.getString(R.string.tts_toast_format),
                    mPercentForBuffering, mPercentForPlaying));
        }

        @Override
        public void onCompleted(SpeechError error) {
            if (error == null) {
                Log.e(TAG,"播放完成");
            } else {
                Log.e(TAG,error.getPlainDescription(true));
            }
            release();
        }

        @Override
        public void onEvent(int eventType, int arg1, int arg2, Bundle obj) {
            // 以下代码用于获取与云端的会话id，当业务出错时将会话id提供给技术支持人员，可用于查询会话日志，定位出错原因
            // 若使用本地能力，会话id为null
            //	if (SpeechEvent.EVENT_SESSION_ID == eventType) {
            //		String sid = obj.getString(SpeechEvent.KEY_EVENT_SESSION_ID);
            //		Log.d(TAG, "session id =" + sid);
            //	}

            //实时音频流输出参考
			/*if (SpeechEvent.EVENT_TTS_BUFFER == eventType) {
				byte[] buf = obj.getByteArray(SpeechEvent.KEY_EVENT_TTS_BUFFER);
				Log.e("MscSpeechLog", "buf is =" + buf);
			}*/
        }
    };

    private void setParam(){
       if(mTts==null)return;
        mTts.setParameter(SpeechConstant.PARAMS, null);// 清空参数
        //设置合成
        if(mEngineType.equals(SpeechConstant.TYPE_CLOUD)) {
            mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);//设置使用云端引擎
            mTts.setParameter(SpeechConstant.VOICE_NAME,voicerCloud);//设置发音人
        }else {
            mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_LOCAL);//设置使用本地引擎
            mTts.setParameter(ResourceUtil.TTS_RES_PATH,getResourcePath());//设置发音人资源路径
            mTts.setParameter(SpeechConstant.VOICE_NAME,voicerLocal);//设置发音人
        }
        //mTts.setParameter(SpeechConstant.TTS_DATA_NOTIFY,"1");//支持实时音频流抛出，仅在synthesizeToUri条件下支持
        mTts.setParameter(SpeechConstant.SPEED, "50");//设置合成语速
        mTts.setParameter(SpeechConstant.PITCH, "50");//设置合成音调
        mTts.setParameter(SpeechConstant.VOLUME, "50");//设置合成音量
        mTts.setParameter(SpeechConstant.STREAM_TYPE, "3");//设置播放器音频流类型
        // 设置播放合成音频打断音乐播放，默认为true
        mTts.setParameter(SpeechConstant.KEY_REQUEST_FOCUS, "true");
        // 设置音频保存路径，保存音频格式支持pcm、wav，设置路径为sd卡请注意WRITE_EXTERNAL_STORAGE权限
        mTts.setParameter(SpeechConstant.AUDIO_FORMAT, "wav");
        mTts.setParameter(SpeechConstant.TTS_AUDIO_PATH, Environment.getExternalStorageDirectory()+"/msc/tts.wav");
    }

    //获取发音人资源路径
    private String getResourcePath(){
        StringBuffer tempBuffer = new StringBuffer();
        //合成通用资源
        tempBuffer.append(ResourceUtil.generateResourcePath(context, ResourceUtil.RESOURCE_TYPE.assets, "tts/common.jet"));
        tempBuffer.append(";");
        //发音人资源
        tempBuffer.append(ResourceUtil.generateResourcePath(context, ResourceUtil.RESOURCE_TYPE.assets, "tts/"+voicerLocal+".jet"));
        return tempBuffer.toString();
    }

    /**
     * 释放资源
     */
    private void release(){
       try {
           if( null != mTts ){
               mTts.stopSpeaking();
               mTts.destroy();//释放连接
           }
       }catch (Exception e){
           e.printStackTrace();
       }
    }
}
