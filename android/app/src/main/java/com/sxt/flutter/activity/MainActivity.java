package com.sxt.flutter.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.iflytek.cloud.SpeechConstant;
import com.sxt.flutter.App;
import com.sxt.flutter.R;
import com.sxt.flutter.alarm.MainService;
import com.sxt.flutter.util.Constants;
import com.sxt.flutter.util.NotificationHelper;
import com.sxt.flutter.util.SpeachUti;

import java.util.Random;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends BaseActivity implements  MethodChannel.MethodCallHandler {

    static final int REQUEST_CODE_RECORD=10086;
    private Handler handler=new Handler();
    private int id=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForegroundService(new Intent(App.getCtx(), MainService.class));
//        } else {
            startService(new Intent(App.getCtx(), MainService.class));
//        }

        MethodChannel channel=new MethodChannel(registrarFor(Constants.DEFAULT_CHANNEL).messenger(), Constants.DEFAULT_CHANNEL);
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        if(methodCall.method.equals("playNotifyMusic")){
            Log.e("notification",""+methodCall.arguments);
            playMusic(String.valueOf(methodCall.arguments));
            handler.postDelayed(() -> {
                NotificationHelper helper=new NotificationHelper(MainActivity.this,"default","defaultName",R.raw.order_message);
                helper.notify(id++,helper.buildNotificationText(getString(R.string.app_name),String.valueOf(methodCall.arguments),null,null,R.mipmap.ic_launcher_round));
            },1000);
        }
    }

    /**
     * 播放语音
     */
    private void playMusic(String text) {
        new SpeachUti(this).startSpeaching(text, SpeechConstant.TYPE_LOCAL);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermission(REQUEST_CODE_RECORD, Manifest.permission.RECORD_AUDIO, new String[]{Manifest.permission.RECORD_AUDIO});
    }

    @Override
    public void onPermissionsRefusedNever(int requestCode, String[] permissions, int[] grantResults) {
        super.onPermissionsRefusedNever(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_RECORD) {
            showPermissionRefusedNeverDialog(String.format("还未获取到相应权限哦,您可以在应用设置中允许%s使用麦克风的权限哟", getString(R.string.app_name)));
        }
    }

    /**
     * 权限被彻底禁止后 , 弹框提醒用户去开启
     */
    private void showPermissionRefusedNeverDialog(CharSequence message) {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        AlertDialog dialog=builder.create();
        dialog.setTitle("提示");
        dialog.setMessage(message);
        dialog.setCancelable(false);
        builder.setNegativeButton("取消", (dialog1, which) -> dialog1.dismiss());
        builder.setPositiveButton("确定", (dialog12, which) -> {
            dialog12.dismiss();
            goToAppSettingsPage();
        });
    }
}


