package com.sxt.flutter.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import io.flutter.app.FlutterActivity;

/**
 * Created by sxt on 2018/1/9.
 */

public class BaseActivity extends FlutterActivity {

    protected String TAG = this.getClass().getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public boolean checkPermission(int requestCode, String permssion, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, permssion) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(permissions, requestCode);
                return false;
            }
            return true;
        }
        return true;
    }

    public void goToAppSettingsPage() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivity(intent);
        finish();
    }

    private boolean hasAllPermissionsGranted(int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (hasAllPermissionsGranted(grantResults)) {
            onPermissionsaAlowed(requestCode, permissions, grantResults);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!shouldShowRequestPermissionRationale(permissions[0])) {
                    onPermissionsRefusedNever(requestCode, permissions, grantResults);
                } else {
                    onPermissionsRefused(requestCode, permissions, grantResults);
                }
            }
        }
    }

    public void onPermissionsaAlowed(int requestCode, String[] permissions, int[] grantResults) {
        Log.e(TAG, "onPermissionsaAlowed --> requestCode:" + requestCode);
    }

    public void onPermissionsRefused(int requestCode, String[] permissions, int[] grantResults) {
        Log.e(TAG, "onPermissionsRefused --> requestCode:" + requestCode);
    }

    public void onPermissionsRefusedNever(int requestCode, String[] permissions, int[] grantResults) {
        Log.e(TAG, "onPermissionsRefusedNever --> requestCode:" + requestCode);
    }
}
