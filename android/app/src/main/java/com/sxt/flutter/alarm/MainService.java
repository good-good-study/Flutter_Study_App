package com.sxt.flutter.alarm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.sxt.flutter.App;
import com.sxt.flutter.R;
import com.sxt.flutter.activity.MainActivity;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by sxt on 2018/8/1.
 */

public class MainService extends Service {

    private final String TAG = this.getClass().getName();
    public static final int NOTIFY_ID = 9892;
    private final long jobPeriodic = 3000;
    private JobScheduler jobScheduler;
    private Timer timer;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        forgroundService();
        startJobScheduler();
        AlarmTaskManager.getInstance(this).createAlarm();
        startTimer();
        Log.e(TAG, "MainService, created");
        return START_STICKY;
    }

    private void forgroundService() {
        Notification.Builder builder;
        Intent in = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, PendingIntent.FLAG_UPDATE_CURRENT, in, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            CharSequence name = "Flutter";
            String description = "Flutter Study";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel mChannel = new NotificationChannel(String.valueOf(NOTIFY_ID), name, importance);
            mChannel.setSound(null, null);
            mChannel.enableVibration(false);
            mChannel.setDescription(description);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mChannel);
            }
            builder = new Notification.Builder(this, String.valueOf(NOTIFY_ID));
        } else {
            builder = new Notification.Builder(this);
        }
        startForeground(NOTIFY_ID, builder.setSmallIcon(R.mipmap.ic_launcher_round)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Flutter Study , created by sxt at 2019/5/13")
                .setContentIntent(pendingIntent)
                .build());
    }

    private void startJobScheduler() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (jobScheduler == null) {
                jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
            }
            JobInfo.Builder jobInfoBuild = new JobInfo.Builder(10086, new ComponentName(getPackageName(), MyJobService.class.getName()));
            jobInfoBuild.setPeriodic(jobPeriodic);//重复执行的间隔
            jobScheduler.schedule(jobInfoBuild.build());
        }
    }

    private void startTimer() {
        if (timer == null) {
            timer = new Timer();
        } else {
            timer.cancel();
            timer = new Timer();
        }
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                WorkManager.getInstance().enqueue(new OneTimeWorkRequest.Builder(JPushServiceMonitor.class).build());
            }
        }, 0, 20000);
    }

    @Override
    public void onDestroy() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForegroundService(new Intent(App.getCtx(), SecondService.class));
//        } else {
            startService(new Intent(App.getCtx(), SecondService.class));
//        }
        if (timer != null) timer.cancel();
        Log.e(TAG, "MainService, onDestroy");
        super.onDestroy();
    }
}
