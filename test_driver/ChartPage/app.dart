import 'package:com.sxt.flutter/net/FlutterService.dart';
import 'package:com.sxt.flutter/pages/ChartApp.dart';
import 'package:flutter/material.dart';
import 'package:flutter_driver/driver_extension.dart';

void main() async {
  enableFlutterDriverExtension();
  FlutterService().start();
  runApp(ChartApp());
}
