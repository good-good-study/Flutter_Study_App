// Imports the Flutter Driver API
import 'dart:async';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

main({FlutterDriver driver}) async {
  if (driver == null) {
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
  }
  //可以在这里设置 测试完成后 自动关闭连接 ,结束测试
//  tearDownAll(() async {
//    if (driver != null) {
//      await driver.close();
//    }
//  });
  group('测试GridViewApp的列表滚动', () {
    test('验证列表是否包含特定项目', () async {
      //通过key查找widget
      final listFinder = find.byValueKey('gridView');
      final itemFinderFirst = find.byValueKey('0');
      final itemFinderLast = find.byValueKey('11');

      await driver.scrollUntilVisible(
        listFinder,
        itemFinderLast,
        dyScroll: -200,
      );
      expect(
        await driver.getText(itemFinderLast),
        '吃点儿啥',
      );
      await driver.scrollUntilVisible(
        listFinder,
        itemFinderFirst,
        dyScroll: 200,
      );
      expect(
        await driver.getText(itemFinderFirst),
        '假如生活欺骗了你',
      );
    });
    test('测试GridView上下滑动', () async {
      final listFinder = find.byValueKey('gridView');
      await driver.scroll(listFinder, 0, -500, Duration(seconds: 1));
      await driver.scroll(listFinder, 0, 500, Duration(seconds: 1));
    });
  });
}
