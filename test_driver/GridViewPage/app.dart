import 'package:com.sxt.flutter/net/FlutterService.dart';
import 'package:com.sxt.flutter/pages//GridView/page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_driver/driver_extension.dart';

void main() async {
  // This line enables the extension
  enableFlutterDriverExtension();

  await FlutterService().start(); //开启grpc服务端
  //不要在这里进行网络操作,会阻塞,可以在界面里面去请求数据,不会造成阻塞
  runApp(GridViewPage().buildPage(null)); //运行要测试的界面
}
