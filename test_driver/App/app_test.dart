import 'dart:async';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

import '../TabViewPage/app_test.dart' as TabViewPage;
import '../GridViewPage/app_test.dart' as GridViewPage;
import '../ChartPage/app_test.dart' as ChartPage;
import '../MyPage//app_test.dart' as MyPage;

void main() async {
  FlutterDriver driver;
  setUpAll(() async {
    driver = await FlutterDriver.connect();
  });
//  tearDownAll(() async {
//    if (driver != null) {
//      当测试完成后,是否要关闭连接
//      await driver.close();
//    }
//  });
  group('测试首页', () {
    TabViewPage.main(driver: driver);
  });
  group('测试GridView', () {
    test('切换tab -> 时下流行', () async {
      final tabFinder = find.byValueKey('时下流行');
      await driver.tap(tabFinder);
      Future.delayed(Duration(seconds: 2));
    });
    GridViewPage.main(driver: driver);
  });
  group('测试Chart', () {
    test('切换tab -> 视频', () async {
      final tabFinder = find.byValueKey('视频');
      await driver.tap(tabFinder);
      await Future.delayed(Duration(seconds: 2));
    });
    ChartPage.main(driver: driver);
  });
  group('测试我的', () {
    test('切换tab -> 我的', () async {
      final tabFinder = find.byValueKey('我的');
      await driver.tap(tabFinder);
      await Future.delayed(Duration(seconds: 2));
    });
    MyPage.main(driver: driver);
  });
}
