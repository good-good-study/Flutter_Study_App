import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

///每个group测试时长最多30秒,不然会crash,并给出TimeOut异常
main({FlutterDriver driver}) async {
  if (driver == null) {
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
  }
//  tearDownAll(() async {
//    if (driver != null) {
  //当测试完成后,是否要关闭连接
//      await driver.close();
//    }
//  });
  group('测试Tab界面滑动', () {
    test('测试tabBar左右滑动', () async {
      final listFinder = find.byValueKey('tabBar');
      await driver.scroll(listFinder, -150, 0, Duration(seconds: 1));
      await driver.scroll(listFinder, 150, 0, Duration(seconds: 1));
    });
    test('测试ListView上下滑动', () async {
      final listFinder = find.byValueKey('listView0');
      await driver.scroll(listFinder, 0, -500, Duration(seconds: 1));
      await driver.scroll(listFinder, 0, 500, Duration(seconds: 1));
    });
    test('测试TabBarView左右滑动', () async {
      final listFinder = find.byValueKey('tabBarView');
      const num length = 6;
      const double interval = 300;
      for (int i = 0; i < length; i++) {
        await driver.scroll(
            listFinder, -interval, 0, Duration(milliseconds: 500));
      }
      for (int i = 0; i < length; i++) {
        await driver.scroll(
            listFinder, interval, 0, Duration(milliseconds: 500));
      }
    });
  });
}
