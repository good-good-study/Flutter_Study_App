import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

///每个group测试时长最多30秒,不然会crash,并给出TimeOut异常
main({FlutterDriver driver}) async {
  if (driver == null) {
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
  }
//  tearDownAll(() async {
//    if (driver != null) {
  //当测试完成后,是否要关闭连接
//      await driver.close();
//    }
//  });

  test('测试我的 ListView上下滑动', () async {
    final listFinder = find.byValueKey('my_listView');
    await driver.scroll(listFinder, 0, -500, Duration(seconds: 1));
    await driver.scroll(listFinder, 0, 500, Duration(seconds: 1));
  });
}
