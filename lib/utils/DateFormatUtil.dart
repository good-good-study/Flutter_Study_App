import 'package:intl/intl.dart';

class DateFormatUtil {
  static const String DATE_HH_MM = 'HH:mm';
  static const String DATE_HH_MM_SS = 'HH:mm:ss';
  static const String DATE_YY = 'yyyy';
  static const String DATE_MM = 'MM';
  static const String DATE_DD = 'dd';
  static const String DATE_MM_DD = 'MM-dd';
  static const String DATE_YY_MM = 'yyyy-MM';
  static const String DATE_YY_MM_DD = 'yyyy-MM-dd';
  static const String DATE_ALL = 'yyyy-MM-dd HH:mm:ss';

  ///毫秒值转换成日期
  static String getDateFromSeconds(num seconds, String flag) {
    var format = new DateFormat(flag);
    return format.format(DateTime.fromMillisecondsSinceEpoch(seconds));
  }

  ///日期转换为毫秒值
  static String getSecondsFromDate(String date) {
    var format = new DateFormat(DATE_ALL);
    return format.parse(date).toString();
  }

  ///根据毫秒值计算视频时长 return -> hh:mm:ss
  ///flag : 默认为hh:mm:ss ; 另可选 mm:ss || hh:mm:ss
  static String getTimeFromDuration(Duration duration, {String flag}) {
    double minutes = duration.inMilliseconds / (60 * 1000); //分钟数
    int hours = minutes ~/ 60; //小时数
    int mins = (duration.inMilliseconds - hours * 3600 * 1000) ~/ (1000 * 60);
    int seconds = (duration.inMilliseconds - mins * 60 * 1000) ~/ 1000;
    if (DATE_HH_MM == flag) {
      int minuties = minutes.toInt();
      return '${minuties == 0 ? "00" : (minuties < 10 ? "0$minuties" : minuties)}:${seconds < 10 ? "0$seconds" : seconds}';
    } else {
      return '${hours == 0 ? "00" : (hours < 10 ? "0$hours" : hours)}:'
          '${mins == 0 ? "00" : (mins < 10 ? "0$mins" : mins)}:${seconds < 10 ? "0$seconds" : seconds}';
    }
  }
}
