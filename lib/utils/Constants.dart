class Constants {
  static const String videoUrl =
      'http://video.izhaohu.com/21f560d483d441bea922dfe7631cdd3b/d20a37ab6673409fb5279361b71b90c3-b1652e9b90f6fbca44d3573623d2fe6b-ld.mp4';

  ///画江湖
  static const String url0 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/70ba14c74057fca8806419e8b85f3cca.jpg';
  static const String url1 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/463a9dfe40d18f71809c3a7efcb186da.jpg';
  static const String url2 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/63c9df364007fd8380d18ba8c5d24240.jpg';
  static const String url3 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/3d66c75f40c3778a8050fd450f880cff.jpg';
  static const String url4 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/ab2f31084017e0518065ff24591c1458.jpg';
  static const String url5 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/4968f701405d6840807d741c5921ab50.jpg';
  static const String url6 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/394ece2540ef50c480a14a658cfddb18.jpg';
  static const String url7 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/2c1cde0540dff55380549b762fb07e88.jpg';
  static const String url8 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/addc9856403465f880362cc94f610e39.jpg';
  static const String url9 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/fd3f316d40808333801d38369adc8d86.jpg';

  ///少年歌行
  static const String url10 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/95d7676e40dc78d880380e64631f1866.webp';
  static const String url11 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/ee4bbe8d40d0f8328085bc9c00ef5839.webp';
  static const String url12 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/8d9c85a240e7d2328000b9e11126faf8.webp';
  static const String url13 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/daf8c2a240cb972f800b9f6b424a0762.webp';
  static const String url14 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/0261629b401147b8807f352b39ec7bcd.webp';
  static const String url15 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/742bdc6b40138d8180e68b5be53a5e99.webp';
  static const String url16 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/c7b5f79c4035345d8092092fdba47f25.webp';
  static const String url17 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/34f4afc440e5829780e9b9ffc1b88f78.webp';
  static const String url18 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/18964015403dfc5e8012e7ce547aca05.webp';
  static const String url19 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/8939178940943f5f80313445e5d7fc19.webp';

  ///侠岚
  static const String url20 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/653dec8340e3a65f80bc2567a4880d2b.jpeg';
  static const String url21 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/f28487ec4004403d8080378c14ad2301.jpg';
  static const String url22 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/c07dfecb4014335e805c20151a0ef75d.jpg';
  static const String url23 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/1c53d5ca402bb3c7801e75103dda7a30.jpg';
  static const String url24 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/af206efb4054f8d880e2712af8c6e1cf.jpg';

  ///风景
  static const String url25 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/48c0c0bf40e0bce2807b206d8b7d9a6d.jpg';
  static const String url26 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/6f2d78b8405a6a4580c8efca8881a44a.jpg';
  static const String url27 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/12630b734053de3280c5fe07308e6e8e.jpg';
  static const String url28 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/226a309e403310888030c65a4712dc36.jpg';
  static const String url29 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/23f5670140fdc19b80384c3f8fceb0cb.jpg';
  static const String url30 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/24c824df404d1c3a80bb8c9d6c2863bb.jpg';
  static const String url31 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/d15067d7401633f680c18cdfb2250c5f.jpg';
  static const String url32 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/410da733400652ac80f26dc6cbb1ea0f.jpg';

  ///换世门主
  static const String url33 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/267516d9404002cf80119866f13a080d.jpg';
  static const String url34 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/c0bff733409f4f7e8043efd00b399fef.jpg';
  static const String url35 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/7a4602c740f975f08037ffe13e39e57b.jpg';
  static const String url36 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/5321565e4087f45c8026da6029ebf4c0.jpg';
  static const String url37 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/55c2c7a94048f9b280f5daedf3e3aea8.jpg';

  ///血色苍穹
  static const String url38 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/3d0d2a8b406f6b5a80e0fb334222cf0e.jpeg';
  static const String url39 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/aa1a654c40faaa7f804ecfc7c445c22a.jpeg';
  static const String url40 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/b8d0252140e2700a803a91e07066b169.jpg';
  static const String url41 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/4c0dbec240c3194180cefaa8068c7d4c.jpg';
  static const String url42 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/b2e85d52406db864800a7858e8e3b289.jpg';
  static const String url43 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/a001df6740c979f680b6dac7e6fc8891.jpg';
  static const String url44 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/dc3f98df404c611780c1a74384751b2d.jpg';
  static const String url45 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/9c538302403901a380ab123223cf5730.jpg';

  ///妖神记
  static const String url46 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/ae1c3e3d40b1d3ca8052887b36353a58.jpg';
  static const String url47 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/86b46a2340bb40fd80e43a85d7878bdc.jpeg';
  static const String url48 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/145b8f9240fe58a78015987cb205ae0f.jpg';
  static const String url49 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/ae19c21e40d049a780bd35f0854591c0.jpg';
  static const String url50 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/25dfb1884036d73880ce42c6b943feab.jpg';
  static const String url51 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/5088b392409a2e588064fde26bbba67b.jpg';
  static const String url52 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/9b692e2e4028cdce801c1ea0029fee91.jpg';
  static const String url53 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/fa55d3cc40dfabb68019c83f6b6ee3c9.jpg';
  static const String url54 =
      'http://bmob-cdn-25616.b0.upaiyun.com/2019/05/11/e320fb1c40fb5d5e8073410f50be5751.jpg';

  ///我的主页
  static String home_page_csdn = 'https://blog.csdn.net/sxt_zls';
  static String home_page_github = 'https://github.com/good-good-study';
}
