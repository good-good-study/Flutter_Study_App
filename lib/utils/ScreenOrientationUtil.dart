import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:orientation/orientation.dart';

// 这种方式不适配IOS设备
//    return SystemChrome.setPreferredOrientations([
//          DeviceOrientation.landscapeLeft,
//          DeviceOrientation.landscapeRight,
//        ]).then((_) {
//          print('当前为横屏');
//          return Orientation.landscape;
//        });
//        return SystemChrome.setPreferredOrientations(
//                [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown])
//            .then((_) {
//          print('当前为竖屏');
//          return Orientation.portrait;
//        });
class ScreenOrientationUtil {
  ///切换屏幕方向,最终返回当前的屏幕方向
  static Future<Orientation> setScreenorientation(bool isPortrait) async {
    {
      if (isPortrait) {
        await OrientationPlugin.forceOrientation(
            DeviceOrientation.landscapeLeft);
        return Orientation.landscape;
      } else {
        await OrientationPlugin.forceOrientation(DeviceOrientation.portraitUp);
        return Orientation.portrait;
      }
    }
  }
}
