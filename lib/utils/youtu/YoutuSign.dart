import 'dart:convert';
import 'dart:math';

import 'package:crypto/crypto.dart';

class YoutuSign {
  static int appSign(String appId, String secret_id, String secret_key,
      num expired, String userid, StringBuffer mySign) {
    return appSignBase(appId, secret_id, secret_key, expired, userid, mySign);
  }

  static int appSignBase(String appId, String secret_id, String secret_key,
      num expired, String userid, StringBuffer mySign) {
    if (!empty(secret_id) && !empty(secret_key)) {
      String puserid = "";
      if (!empty(userid)) {
        if (userid.length > 64) {
          return -2;
        }

        puserid = userid;
      }

      num now = DateTime.now().millisecondsSinceEpoch / 1000;
      int rdm = Random().nextInt(10000);
      String plain_text =
          'a=$appId&k=$secret_id&e=$expired&t=$now&r=$rdm&u=$puserid';

      List<int> bin = hashHmac(plain_text, secret_key);
      var addContent = utf8.encode(plain_text).toList();
//        int totalLength = bin.length + addContent.length;
      List<int> all = new List();
//        List.copyRange(bin, 0, all, 0, bin.length);
      all.addAll(bin);
      all.addAll(addContent);
      mySign.write(base64Encode(all));
//        List.copyRange(addContent, 0, all, bin.length, addContent.length);
      return 0;
    } else {
      return -1;
    }
  }

  static List hashHmac(String plain_text, String accessKey) {
    var key = utf8.encode(accessKey);
    var bytes = utf8.encode(plain_text);
    var hmacSha256 = new Hmac(sha256, key); // HMAC-SHA256
    var digest = hmacSha256.convert(bytes);
    print('加密后:${digest.toString()}');
    return digest.bytes;
  }

  static bool empty(String s) {
    return s == null || s.trim().length == 0;
  }
}
