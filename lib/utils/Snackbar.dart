import 'package:flutter/material.dart';

class SnackBarUtil {
  static GlobalKey<ScaffoldState> _scaffoldKey;

  static void showSnackBar(GlobalKey<ScaffoldState> scaffoldKey, String msg) {
    if (SnackBarUtil._scaffoldKey != null &&
        SnackBarUtil._scaffoldKey.currentState != null) {
      SnackBarUtil._scaffoldKey.currentState.removeCurrentSnackBar();
    }
    if (scaffoldKey != null && msg != null) {
      SnackBarUtil._scaffoldKey = scaffoldKey;
      scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(msg),
        duration: Duration(milliseconds: 800),
      ));
    }
  }
}
