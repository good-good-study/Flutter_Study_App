///
//  Generated code. Do not modify.
//  source: VideoInfo.proto
///
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name

import 'dart:core' as $core
    show bool, Deprecated, double, int, List, Map, override, String;

import 'package:protobuf/protobuf.dart' as $pb;

class VideoInfo extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i =
      $pb.BuilderInfo('VideoInfo', package: const $pb.PackageName('chart'))
        ..aOS(1, 'id')
        ..aOS(2, 'title')
        ..aOS(3, 'description')
        ..aOS(4, 'imageUrl')
        ..aOS(5, 'videoUrl')
        ..hasRequiredFields = false;

  VideoInfo() : super();
  VideoInfo.fromBuffer($core.List<$core.int> i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromBuffer(i, r);
  VideoInfo.fromJson($core.String i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromJson(i, r);
  VideoInfo clone() => VideoInfo()..mergeFromMessage(this);
  VideoInfo copyWith(void Function(VideoInfo) updates) =>
      super.copyWith((message) => updates(message as VideoInfo));
  $pb.BuilderInfo get info_ => _i;
  static VideoInfo create() => VideoInfo();
  VideoInfo createEmptyInstance() => create();
  static $pb.PbList<VideoInfo> createRepeated() => $pb.PbList<VideoInfo>();
  static VideoInfo getDefault() => _defaultInstance ??= create()..freeze();
  static VideoInfo _defaultInstance;

  $core.String get id => $_getS(0, '');
  set id($core.String v) {
    $_setString(0, v);
  }

  $core.bool hasId() => $_has(0);
  void clearId() => clearField(1);

  $core.String get title => $_getS(1, '');
  set title($core.String v) {
    $_setString(1, v);
  }

  $core.bool hasTitle() => $_has(1);
  void clearTitle() => clearField(2);

  $core.String get description => $_getS(2, '');
  set description($core.String v) {
    $_setString(2, v);
  }

  $core.bool hasDescription() => $_has(2);
  void clearDescription() => clearField(3);

  $core.String get imageUrl => $_getS(3, '');
  set imageUrl($core.String v) {
    $_setString(3, v);
  }

  $core.bool hasImageUrl() => $_has(3);
  void clearImageUrl() => clearField(4);

  $core.String get videoUrl => $_getS(4, '');
  set videoUrl($core.String v) {
    $_setString(4, v);
  }

  $core.bool hasVideoUrl() => $_has(4);
  void clearVideoUrl() => clearField(5);
}
