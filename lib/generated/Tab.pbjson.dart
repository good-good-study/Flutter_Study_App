///
//  Generated code. Do not modify.
//  source: Tab.proto
///
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name

const Tab$json = const {
  '1': 'Tab',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {
      '1': 'items',
      '3': 4,
      '4': 3,
      '5': 11,
      '6': '.chart.Item',
      '10': 'items'
    },
  ],
};

const Item$json = const {
  '1': 'Item',
  '2': const [
    const {'1': 'url', '3': 1, '4': 1, '5': 9, '10': 'url'},
  ],
};
