///
//  Generated code. Do not modify.
//  source: Service.proto
///
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name

const TabReq$json = const {
  '1': 'TabReq',
  '2': const [
    const {'1': 'time', '3': 1, '4': 1, '5': 3, '10': 'time'},
  ],
};

const TabResp$json = const {
  '1': 'TabResp',
  '2': const [
    const {
      '1': 'tabs',
      '3': 2,
      '4': 3,
      '5': 11,
      '6': '.chart.Tab',
      '10': 'tabs'
    },
  ],
};

const VideoReq$json = const {
  '1': 'VideoReq',
  '2': const [
    const {'1': 'time', '3': 1, '4': 1, '5': 3, '10': 'time'},
  ],
};

const VideoResp$json = const {
  '1': 'VideoResp',
  '2': const [
    const {
      '1': 'videos',
      '3': 1,
      '4': 3,
      '5': 11,
      '6': '.chart.VideoInfo',
      '10': 'videos'
    },
  ],
};
