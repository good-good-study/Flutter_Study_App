///
//  Generated code. Do not modify.
//  source: Service.proto
///
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'package:grpc/service_api.dart' as $grpc;

import 'dart:core' as $core show int, String, List;

import 'Service.pb.dart';
export 'Service.pb.dart';

class FlutterServiceClient extends $grpc.Client {
  static final _$getTabs = $grpc.ClientMethod<TabReq, TabResp>(
      '/chart.FlutterService/getTabs',
      (TabReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => TabResp.fromBuffer(value));
  static final _$getVideos = $grpc.ClientMethod<VideoReq, VideoResp>(
      '/chart.FlutterService/getVideos',
      (VideoReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => VideoResp.fromBuffer(value));

  FlutterServiceClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<TabResp> getTabs(TabReq request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$getTabs, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<VideoResp> getVideos(VideoReq request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$getVideos, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class FlutterServiceBase extends $grpc.Service {
  $core.String get $name => 'chart.FlutterService';

  FlutterServiceBase() {
    $addMethod($grpc.ServiceMethod<TabReq, TabResp>(
        'getTabs',
        getTabs_Pre,
        false,
        false,
        ($core.List<$core.int> value) => TabReq.fromBuffer(value),
        (TabResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<VideoReq, VideoResp>(
        'getVideos',
        getVideos_Pre,
        false,
        false,
        ($core.List<$core.int> value) => VideoReq.fromBuffer(value),
        (VideoResp value) => value.writeToBuffer()));
  }

  $async.Future<TabResp> getTabs_Pre(
      $grpc.ServiceCall call, $async.Future request) async {
    return getTabs(call, await request);
  }

  $async.Future<VideoResp> getVideos_Pre(
      $grpc.ServiceCall call, $async.Future request) async {
    return getVideos(call, await request);
  }

  $async.Future<TabResp> getTabs($grpc.ServiceCall call, TabReq request);
  $async.Future<VideoResp> getVideos($grpc.ServiceCall call, VideoReq request);
}
