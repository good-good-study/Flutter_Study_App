///
//  Generated code. Do not modify.
//  source: Tab.proto
///
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name

import 'dart:core' as $core
    show bool, Deprecated, double, int, List, Map, override, String;

import 'package:protobuf/protobuf.dart' as $pb;

class Tab extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i =
      $pb.BuilderInfo('Tab', package: const $pb.PackageName('chart'))
        ..aOS(1, 'id')
        ..aOS(2, 'title')
        ..aOS(3, 'description')
        ..pc<Item>(4, 'items', $pb.PbFieldType.PM, Item.create)
        ..hasRequiredFields = false;

  Tab() : super();
  Tab.fromBuffer($core.List<$core.int> i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromBuffer(i, r);
  Tab.fromJson($core.String i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromJson(i, r);
  Tab clone() => Tab()..mergeFromMessage(this);
  Tab copyWith(void Function(Tab) updates) =>
      super.copyWith((message) => updates(message as Tab));
  $pb.BuilderInfo get info_ => _i;
  static Tab create() => Tab();
  Tab createEmptyInstance() => create();
  static $pb.PbList<Tab> createRepeated() => $pb.PbList<Tab>();
  static Tab getDefault() => _defaultInstance ??= create()..freeze();
  static Tab _defaultInstance;

  $core.String get id => $_getS(0, '');
  set id($core.String v) {
    $_setString(0, v);
  }

  $core.bool hasId() => $_has(0);
  void clearId() => clearField(1);

  $core.String get title => $_getS(1, '');
  set title($core.String v) {
    $_setString(1, v);
  }

  $core.bool hasTitle() => $_has(1);
  void clearTitle() => clearField(2);

  $core.String get description => $_getS(2, '');
  set description($core.String v) {
    $_setString(2, v);
  }

  $core.bool hasDescription() => $_has(2);
  void clearDescription() => clearField(3);

  $core.List<Item> get items => $_getList(3);
}

class Item extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i =
      $pb.BuilderInfo('Item', package: const $pb.PackageName('chart'))
        ..aOS(1, 'url')
        ..hasRequiredFields = false;

  Item() : super();
  Item.fromBuffer($core.List<$core.int> i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromBuffer(i, r);
  Item.fromJson($core.String i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromJson(i, r);
  Item clone() => Item()..mergeFromMessage(this);
  Item copyWith(void Function(Item) updates) =>
      super.copyWith((message) => updates(message as Item));
  $pb.BuilderInfo get info_ => _i;
  static Item create() => Item();
  Item createEmptyInstance() => create();
  static $pb.PbList<Item> createRepeated() => $pb.PbList<Item>();
  static Item getDefault() => _defaultInstance ??= create()..freeze();
  static Item _defaultInstance;

  $core.String get url => $_getS(0, '');
  set url($core.String v) {
    $_setString(0, v);
  }

  $core.bool hasUrl() => $_has(0);
  void clearUrl() => clearField(1);
}
