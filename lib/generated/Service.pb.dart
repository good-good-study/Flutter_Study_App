///
//  Generated code. Do not modify.
//  source: Service.proto
///
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name

import 'dart:core' as $core
    show bool, Deprecated, double, int, List, Map, override, String;

import 'package:fixnum/fixnum.dart';
import 'package:protobuf/protobuf.dart' as $pb;

import 'Tab.pb.dart' as $0;
import 'VideoInfo.pb.dart' as $1;

class TabReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i =
      $pb.BuilderInfo('TabReq', package: const $pb.PackageName('chart'))
        ..aInt64(1, 'time')
        ..hasRequiredFields = false;

  TabReq() : super();
  TabReq.fromBuffer($core.List<$core.int> i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromBuffer(i, r);
  TabReq.fromJson($core.String i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromJson(i, r);
  TabReq clone() => TabReq()..mergeFromMessage(this);
  TabReq copyWith(void Function(TabReq) updates) =>
      super.copyWith((message) => updates(message as TabReq));
  $pb.BuilderInfo get info_ => _i;
  static TabReq create() => TabReq();
  TabReq createEmptyInstance() => create();
  static $pb.PbList<TabReq> createRepeated() => $pb.PbList<TabReq>();
  static TabReq getDefault() => _defaultInstance ??= create()..freeze();
  static TabReq _defaultInstance;

  Int64 get time => $_getI64(0);
  set time(Int64 v) {
    $_setInt64(0, v);
  }

  $core.bool hasTime() => $_has(0);
  void clearTime() => clearField(1);
}

class TabResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i =
      $pb.BuilderInfo('TabResp', package: const $pb.PackageName('chart'))
        ..pc<$0.Tab>(2, 'tabs', $pb.PbFieldType.PM, $0.Tab.create)
        ..hasRequiredFields = false;

  TabResp() : super();
  TabResp.fromBuffer($core.List<$core.int> i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromBuffer(i, r);
  TabResp.fromJson($core.String i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromJson(i, r);
  TabResp clone() => TabResp()..mergeFromMessage(this);
  TabResp copyWith(void Function(TabResp) updates) =>
      super.copyWith((message) => updates(message as TabResp));
  $pb.BuilderInfo get info_ => _i;
  static TabResp create() => TabResp();
  TabResp createEmptyInstance() => create();
  static $pb.PbList<TabResp> createRepeated() => $pb.PbList<TabResp>();
  static TabResp getDefault() => _defaultInstance ??= create()..freeze();
  static TabResp _defaultInstance;

  $core.List<$0.Tab> get tabs => $_getList(0);
}

class VideoReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i =
      $pb.BuilderInfo('VideoReq', package: const $pb.PackageName('chart'))
        ..aInt64(1, 'time')
        ..hasRequiredFields = false;

  VideoReq() : super();
  VideoReq.fromBuffer($core.List<$core.int> i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromBuffer(i, r);
  VideoReq.fromJson($core.String i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromJson(i, r);
  VideoReq clone() => VideoReq()..mergeFromMessage(this);
  VideoReq copyWith(void Function(VideoReq) updates) =>
      super.copyWith((message) => updates(message as VideoReq));
  $pb.BuilderInfo get info_ => _i;
  static VideoReq create() => VideoReq();
  VideoReq createEmptyInstance() => create();
  static $pb.PbList<VideoReq> createRepeated() => $pb.PbList<VideoReq>();
  static VideoReq getDefault() => _defaultInstance ??= create()..freeze();
  static VideoReq _defaultInstance;

  Int64 get time => $_getI64(0);
  set time(Int64 v) {
    $_setInt64(0, v);
  }

  $core.bool hasTime() => $_has(0);
  void clearTime() => clearField(1);
}

class VideoResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i =
      $pb.BuilderInfo('VideoResp', package: const $pb.PackageName('chart'))
        ..pc<$1.VideoInfo>(1, 'videos', $pb.PbFieldType.PM, $1.VideoInfo.create)
        ..hasRequiredFields = false;

  VideoResp() : super();
  VideoResp.fromBuffer($core.List<$core.int> i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromBuffer(i, r);
  VideoResp.fromJson($core.String i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromJson(i, r);
  VideoResp clone() => VideoResp()..mergeFromMessage(this);
  VideoResp copyWith(void Function(VideoResp) updates) =>
      super.copyWith((message) => updates(message as VideoResp));
  $pb.BuilderInfo get info_ => _i;
  static VideoResp create() => VideoResp();
  VideoResp createEmptyInstance() => create();
  static $pb.PbList<VideoResp> createRepeated() => $pb.PbList<VideoResp>();
  static VideoResp getDefault() => _defaultInstance ??= create()..freeze();
  static VideoResp _defaultInstance;

  $core.List<$1.VideoInfo> get videos => $_getList(0);
}
