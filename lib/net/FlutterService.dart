import 'dart:async';

import 'package:com.sxt.flutter/generated/Service.pbgrpc.dart';
import 'package:com.sxt.flutter/generated/Tab.pb.dart';
import 'package:com.sxt.flutter/generated/VideoInfo.pb.dart';
import 'package:com.sxt.flutter/utils/Constants.dart';
import 'package:grpc/grpc.dart';

class FlutterService extends FlutterServiceBase {
  Future<void> start() async {
    //创建服务
    var server = Server([new FlutterService()]);
    //启动服务
    await server.serve(port: 10086);
    print('Server -> starting port : ${server.port}');
  }

  @override
  Future<TabResp> getTabs(ServiceCall call, TabReq request) async {
    var tabResp = TabResp();
    _buildTabs(tabResp);
    return tabResp;
  }

  @override
  Future<VideoResp> getVideos(ServiceCall call, VideoReq request) async {
    var videoResp = VideoResp();
    _buildVideos(videoResp);
    return videoResp;
  }

  void _buildVideos(VideoResp videoResp) {
    for (int i = 0; i < 12; i++) {
      var videoInfo = VideoInfo()
        ..id = '$i'
        ..title = '吃点儿啥'
        ..description = '普希金'
        ..videoUrl = Constants.videoUrl;

      if (i == 0) {
        videoInfo
          ..title = '假如生活欺骗了你'
          ..imageUrl = Constants.url11;
      } else if (i == 1) {
        videoInfo
          ..title = '不要悲伤'
          ..imageUrl = Constants.url12;
      } else if (i == 2) {
        videoInfo
          ..title = '不要心急'
          ..imageUrl = Constants.url13;
      } else if (i == 3) {
        videoInfo..imageUrl = Constants.url14;
      } else if (i == 4) {
        videoInfo
          ..title = '犹如的日子'
          ..imageUrl = Constants.url15;
      } else if (i == 5) {
        videoInfo
          ..title = '终将过去'
          ..imageUrl = Constants.url16;
      } else if (i == 6) {
        videoInfo
          ..title = '快乐的日子'
          ..imageUrl = Constants.url17;
      } else if (i == 7) {
        videoInfo
          ..title = '将要来临'
          ..imageUrl = Constants.url18;
      } else if (i == 8) {
        videoInfo..imageUrl = Constants.url19;
      } else if (i == 9) {
        videoInfo..imageUrl = Constants.url11;
      } else if (i == 10) {
        videoInfo..imageUrl = Constants.url12;
      } else if (i == 11) {
        videoInfo..imageUrl = Constants.url13;
      } else {
        videoInfo..imageUrl = Constants.url14;
      }
      videoResp.videos.add(videoInfo);
    }
  }

  void _buildTabs(TabResp tabResp) {
    var tab0 = new Tab()..title = '画江湖';
    var tab1 = new Tab()..title = '少年歌行';
    var tab2 = new Tab()..title = '侠岚';
    var tab3 = new Tab()..title = '血色苍穹';
    var tab4 = new Tab()..title = '换世门主';
    var tab5 = new Tab()..title = '妖神记';
    var tab6 = new Tab()..title = '风景';

    ///画江湖
    tab0.items.add(Item()..url = Constants.url0);
    tab0.items.add(Item()..url = Constants.url1);
    tab0.items.add(Item()..url = Constants.url2);
    tab0.items.add(Item()..url = Constants.url3);
    tab0.items.add(Item()..url = Constants.url4);
    tab0.items.add(Item()..url = Constants.url5);
    tab0.items.add(Item()..url = Constants.url6);
    tab0.items.add(Item()..url = Constants.url7);
    tab0.items.add(Item()..url = Constants.url8);
    tab0.items.add(Item()..url = Constants.url9);

    ///少年歌行
    tab1.items.add(Item()..url = Constants.url10);
    tab1.items.add(Item()..url = Constants.url11);
    tab1.items.add(Item()..url = Constants.url12);
    tab1.items.add(Item()..url = Constants.url13);
    tab1.items.add(Item()..url = Constants.url14);
    tab1.items.add(Item()..url = Constants.url15);
    tab1.items.add(Item()..url = Constants.url16);
    tab1.items.add(Item()..url = Constants.url17);
    tab1.items.add(Item()..url = Constants.url18);
    tab1.items.add(Item()..url = Constants.url19);

    ///侠岚
    tab2.items.add(Item()..url = Constants.url20);
    tab2.items.add(Item()..url = Constants.url21);
    tab2.items.add(Item()..url = Constants.url22);
    tab2.items.add(Item()..url = Constants.url23);
    tab2.items.add(Item()..url = Constants.url24);

    ///血色苍穹
    tab3.items.add(Item()..url = Constants.url38);
    tab3.items.add(Item()..url = Constants.url39);
    tab3.items.add(Item()..url = Constants.url40);
    tab3.items.add(Item()..url = Constants.url41);
    tab3.items.add(Item()..url = Constants.url42);
    tab3.items.add(Item()..url = Constants.url43);
    tab3.items.add(Item()..url = Constants.url44);
    tab3.items.add(Item()..url = Constants.url45);

    ///换世门主
    tab4.items.add(Item()..url = Constants.url34);
    tab4.items.add(Item()..url = Constants.url35);
    tab4.items.add(Item()..url = Constants.url36);
    tab4.items.add(Item()..url = Constants.url37);
    tab4.items.add(Item()..url = Constants.url33);

    ///妖神记
    tab5.items.add(Item()..url = Constants.url46);
    tab5.items.add(Item()..url = Constants.url47);
    tab5.items.add(Item()..url = Constants.url48);
    tab5.items.add(Item()..url = Constants.url49);
    tab5.items.add(Item()..url = Constants.url50);
    tab5.items.add(Item()..url = Constants.url51);
    tab5.items.add(Item()..url = Constants.url52);
    tab5.items.add(Item()..url = Constants.url53);
    tab5.items.add(Item()..url = Constants.url54);

    ///风景
    tab6.items.add(Item()..url = Constants.url25);
    tab6.items.add(Item()..url = Constants.url26);
    tab6.items.add(Item()..url = Constants.url27);
    tab6.items.add(Item()..url = Constants.url28);
    tab6.items.add(Item()..url = Constants.url29);
    tab6.items.add(Item()..url = Constants.url30);
    tab6.items.add(Item()..url = Constants.url31);
    tab6.items.add(Item()..url = Constants.url32);

    tabResp.tabs.add(tab0);
    tabResp.tabs.add(tab1);
    tabResp.tabs.add(tab2);
    tabResp.tabs.add(tab3);
    tabResp.tabs.add(tab4);
    tabResp.tabs.add(tab5);
    tabResp.tabs.add(tab6);
  }
}
