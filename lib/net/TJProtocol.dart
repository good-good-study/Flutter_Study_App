import 'dart:async';

import 'package:com.sxt.flutter/generated/Service.pbgrpc.dart';
import 'package:com.sxt.flutter/generated/Tab.pb.dart';
import 'package:com.sxt.flutter/generated/VideoInfo.pb.dart';
import 'package:grpc/grpc.dart';
import 'package:grpc/service_api.dart';

class TJProtocol {
  static ClientChannel _channel;
  static FlutterServiceClient _client;
  static TJProtocol _protocol = new TJProtocol();

  static const String _host = '127.0.0.1';
  static const int _port = 10086;

  static TJProtocol instance() {
    if (_client == null) {
      _channel = ClientChannel(_host,
          port: _port,
          options: ChannelOptions(credentials: ChannelCredentials.insecure()));
      _client = FlutterServiceClient(_channel);
    }
    return _protocol;
  }

  Future<List<Tab>> getTabs(int time) async {
    try {
      var response = await _client.getTabs(TabReq());
      return response.tabs;
    } catch (e) {
      print('Exception -> ${e.toString()}');
    }
    return null;
  }

  Future<List<VideoInfo>> getVideos() async {
    try {
      var response = await _client.getVideos(VideoReq());
      return response.videos;
    } catch (e) {
      print('Exception -> ${e.toString()}');
    }
    return null;
  }

  void dispose() {
    _client = null;
    if (_channel != null) _channel.shutdown();
  }
}
