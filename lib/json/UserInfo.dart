import 'package:json_annotation/json_annotation.dart';

part 'UserInfo.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class UserInfo {
  int id;
  int age;
  String imgUrl;
  String phone;
  String userName;
  String birthDay;
  String address;

  UserInfo();

  ///不同的类使用不同的mixin即可
  factory UserInfo.fromJson(Map<String, dynamic> json) =>
      _$UserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoToJson(this);
}
