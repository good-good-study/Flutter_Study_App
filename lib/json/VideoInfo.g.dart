// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'VideoInfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoInfo _$VideoInfoFromJson(Map<String, dynamic> json) {
  return VideoInfo()
    ..id = json['id'] as int
    ..url = json['url'] as String
    ..title = json['title'] as String
    ..status = json['status'] as String;
}

Map<String, dynamic> _$VideoInfoToJson(VideoInfo instance) => <String, dynamic>{
      'id': instance.id,
      'url': instance.url,
      'title': instance.title,
      'status': instance.status
    };
