import 'package:json_annotation/json_annotation.dart';
part 'VideoInfo.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class VideoInfo {
  int id;
  String url;
  String title;
  String status;

  VideoInfo();

  ///不同的类使用不同的mixin即可
  factory VideoInfo.fromJson(Map<String, dynamic> json) =>
      _$VideoInfoFromJson(json);

  Map<String, dynamic> toJson() => _$VideoInfoToJson(this);
}
