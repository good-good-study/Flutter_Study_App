// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserInfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInfo _$UserInfoFromJson(Map<String, dynamic> json) {
  return UserInfo()
    ..id = json['id'] as int
    ..age = json['age'] as int
    ..imgUrl = json['imgUrl'] as String
    ..phone = json['phone'] as String
    ..userName = json['userName'] as String
    ..birthDay = json['birthDay'] as String
    ..address = json['address'] as String;
}

Map<String, dynamic> _$UserInfoToJson(UserInfo instance) => <String, dynamic>{
      'id': instance.id,
      'age': instance.age,
      'imgUrl': instance.imgUrl,
      'phone': instance.phone,
      'userName': instance.userName,
      'birthDay': instance.birthDay,
      'address': instance.address
    };
