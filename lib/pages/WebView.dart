import 'package:com.sxt.flutter/item/Title.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewApp extends StatefulWidget {
  final String url;

  WebViewApp({this.url = 'https://flutterchina.club/'});

  @override
  State<StatefulWidget> createState() => WebViewState();
}

class WebViewState extends State<WebViewApp> {
  bool _isLoading = true;
  WebViewController _controller;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: WillPopScope(
        child: Scaffold(
          appBar: TitleBar(
            rootContainer: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(
                  'Flutter',
                  style: new TextStyle(color: Colors.white, fontSize: 20),
                ),
                new Container(
                  margin: EdgeInsets.only(left: 8),
                  child: _isLoading
                      ? new CupertinoActivityIndicator(
                          radius: 8,
                        )
                      : Container(),
                )
              ],
            ),
          ),
          body: WebView(
            initialUrl: widget.url,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (controller) {
              print('onWebViewCreated');
              _controller = controller;
              setState(() {
                _isLoading = true;
              });
            },
            onPageFinished: (String url) {
              print('onPageFinished : url -> $url');
              setState(() {
                _isLoading = false;
              });
            },
          ),
        ),
        onWillPop: () async {
          print('onWillPop');
//            if (_controller != null) {
//              _controller.canGoBack().then((canGoBack) {
//                if (canGoBack) {
//                  _controller.goBack();
//                  return false;
//                }
//              });
//            }
        },
      ),
    );
  }
}
