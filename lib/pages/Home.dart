import 'package:com.sxt.flutter/item/item_bottom_tabs.dart';
import 'package:com.sxt.flutter/pages/ChartApp.dart';
import 'package:com.sxt.flutter/pages/My.dart';
import 'package:com.sxt.flutter/pages/gridView/page.dart';
import 'package:com.sxt.flutter/pages/tabBar/page.dart';
import 'package:flutter/material.dart';

import 'WebView.dart';

class HomeApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomePageState();
}

class HomePageState extends State<HomeApp> {
  final tab_icon_titles = ['首页', '时下流行', '视频', '我的'];
  final tab_icon_colors = [
    Colors.deepOrange,
    Colors.green,
    Colors.deepPurple,
    Colors.purple
  ];
  final tab_icon_data = [
    Icons.home,
    Icons.subscriptions,
    Icons.add_shopping_cart,
    Icons.person
  ];
  int tab_index = 0;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  static const MaterialColor tab_select_color = Colors.blue;
  static const MaterialColor tab_unselect_color = Colors.grey;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
        key: _scaffoldKey,
        floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: _buildFloatButton(context),
        body: IndexedStack(
          alignment: AlignmentDirectional.bottomCenter,
          index: tab_index,
          children: <Widget>[
            TabBarPage().buildPage(null),
            GridViewPage().buildPage(null),
            ChartApp(),
            ContactsDemo(),
          ],
        ),
        bottomNavigationBar: _buildBootmNavitionBar(),
      ),
    );
  }

  Widget _buildFloatButton(BuildContext context) {
    return new FloatingActionButton(
      highlightElevation: 8,
      backgroundColor: tab_icon_colors[tab_index],
      onPressed: () {
        Navigator.push(context, new MaterialPageRoute(builder: (context) {
          return WebViewApp();
        }));
      },
      isExtended: true,
      child: new Text('float'),
    );
  }

  ///构建底部导航栏
  Widget _buildBootmNavitionBar() {
    return BottomTabsWidget(
      tabIconTitles: tab_icon_titles,
      tabIconColors: tab_icon_colors,
      tabIconData: tab_icon_data,
      type: BottomNavigationBarType.shifting,
      onPressed: (index) {
        setState(() {
          tab_index = index;
        });
      },
    );
  }
}
