import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:com.sxt.flutter/utils/youtu/YouTuSDKConfig.dart';
import 'package:com.sxt.flutter/utils/youtu/YoutuSign.dart';
import 'package:image_picker/image_picker.dart';

class OcrApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => OcrAppState();
}

class OcrAppState extends State<OcrApp> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey _imageKey = new GlobalKey();
  List<File> images = new List();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
            child: Container(
          child: Column(
            children: <Widget>[
              images == null || images.length == 0
                  ? new Container()
                  : Expanded(
                      child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          itemCount: images.length,
                          itemBuilder: (context, index) {
                            return Card(
                              child: Image.file(
                                images[index],
                                key: index == 0 ? _imageKey : null,
                                fit: BoxFit.cover,
                                width: double.infinity,
                                height: 200,
                              ),
                              clipBehavior: Clip.antiAlias,
                              elevation: 16,
                              margin: EdgeInsets.all(8),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                            );
                          }),
                    ),
              RaisedButton(
                  child: Text('选择图片'),
                  onPressed: () {
                    _getImageFromCamera();
                  })
            ],
          ),
        )),
      ),
    );
  }

  Future _getImageFromCamera() async {
//    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      images.add(image);
      _parseImage(image);
    });
  }

  Future _parseImage(File file) async {
//      RenderRepaintBoundary boundary =
//          _imageKey.currentContext.findRenderObject();
//      double pixelRatio = 1.5;
//      ui.Image image = await boundary.toImage(pixelRatio: pixelRatio);
//      ByteData byteData =
//          await image.toByteData(format: ui.ImageByteFormat.png);
//      Uint8List pngBytes = byteData.buffer.asUint8List();
//      var receivePort = new ReceivePort();
//      await Isolate.spawn(decodePng, receivePort.sendPort);
//      var sendPort = await receivePort.first;
//      ImageUtil.Image uImage = await sendReceive(sendPort, pngBytes);
//
//      var receivePort1 = new ReceivePort();
//      await Isolate.spawn(encodePng, receivePort1.sendPort);
//      var sendPort1 = await receivePort1.first;
//      List<int> byteList = await sendReceive(sendPort1, uImage);
    String base64Image = base64Encode(await file.readAsBytes()); //base 64;

    Dio dio = new Dio();
    String baseUrl =
        YouTuSDKConfig.Base_Url + YouTuSDKConfig.TYPE_IMAGE_CARDCOR;
    StringBuffer mySign = new StringBuffer("");
    int EXPIRED_SECONDS = 2592000;
    num time = DateTime.now().millisecondsSinceEpoch / 1000 + EXPIRED_SECONDS;
    YoutuSign.appSign(YouTuSDKConfig.APP_ID, YouTuSDKConfig.SECRET_ID,
        YouTuSDKConfig.SECRET_KEY, time, YouTuSDKConfig.USER_ID, mySign);

//    print('鉴权信息: ${mySign.toString()}');
    mySign.clear();
    mySign.write(
        'YGPh+nR6ExKS7+gAXJukrEIOYzthPTEwMTM2NDUxJms9QUtJRHo1RFBjcmtRaTBKaERpNG5LcUxOUmNtNFd4Tm85cUtvJmU9MTU1NzU0MTE5OSZ0PTE1NTQ5NDkxOTkmcj0xODE1MjQ5MTc2JnU9MjMwOTM5ODkxMw==');
    Response response = await dio.post(
      baseUrl,
      data: {
        "image": base64Image,
        'card_type': 0,
        'app_id': YouTuSDKConfig.APP_ID,
      },
      options:
          new Options(contentType: ContentType.parse("text/json"), headers: {
        "Content-Type": "text/json",
        "accept": "*/*",
        "user-agent": "youtu-java-sdk",
        "Authorization": mySign.toString()
      }),
    );
    var data = response.data;
  }
}
