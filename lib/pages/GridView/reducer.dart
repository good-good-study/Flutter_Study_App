import 'package:com.sxt.flutter/pages/gridView/action.dart';
import 'package:com.sxt.flutter/pages/gridView/state.dart';
import 'package:fish_redux/fish_redux.dart';

class GridViewReducter {
  Reducer<GridViewState> buildReducer() {
    return asReducer({GridViewAction.refresh: _refresh});
  }

  ///刷新数据源
  GridViewState _refresh(GridViewState state, Action action) {
    GridViewState newState = state.clone();
    newState.videos = action.payload == null ? new List() : action.payload;
    return newState;
  }
}
