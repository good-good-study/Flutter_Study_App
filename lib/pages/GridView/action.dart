import 'package:com.sxt.flutter/generated/VideoInfo.pb.dart';
import 'package:fish_redux/fish_redux.dart';

//定义Action类型
enum GridViewAction {
  onRefresh, //准备刷新数据
  refresh, //刷新数据
  onItemClick, //图片点击
}

//定义Action事件
class GridViewActionCreator {
  ///当刷新数据时
  static Action onRefresh() {
    return Action(GridViewAction.onRefresh);
  }

  ///刷新数据
  static Action refresh(List<VideoInfo> videos) {
    return Action(GridViewAction.refresh, payload: videos);
  }

  ///点击事件
  static Action onItemClick(VideoInfo videoInfo) {
    return Action(GridViewAction.onItemClick, payload: videoInfo);
  }
}
