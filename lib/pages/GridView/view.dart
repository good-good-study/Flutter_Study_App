import 'package:com.sxt.flutter/generated/VideoInfo.pb.dart';
import 'package:com.sxt.flutter/item/Title.dart';
import 'package:com.sxt.flutter/item/item_no_data_layout.dart';
import 'package:com.sxt.flutter/pages/gridView/action.dart';
import 'package:com.sxt.flutter/pages/gridView/state.dart';
import 'package:com.sxt.flutter/values/colors.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Widget buildView(
    GridViewState state, Dispatch dispatch, ViewService viewService) {
  return new MaterialApp(
    debugShowMaterialGrid: false,
    debugShowCheckedModeBanner: false,
    home: AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light
          .copyWith(statusBarColor: Colors.transparent),
      child: new Scaffold(
        appBar: TitleBar(
          titleStyle: TextStyle(
              color: ColorStyle.white,
              fontSize: 20,
              fontWeight: FontWeight.bold),
          backgroundColor: Colors.green,
          title: '少年歌行',
          isShowBack: false,
        ),
        body: state.videos == null || state.videos.length == 0
            ? WidgetNoData()
            : GridView(
                key: Key('gridView'),
                physics: BouncingScrollPhysics(),
                padding: EdgeInsets.all(0),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2),
                children: _list(state, dispatch, viewService),
              ),
      ),
    ),
  );
}

List<Widget> _list(
    GridViewState state, Dispatch dispatch, ViewService viewService) {
  List<Widget> list = new List<Widget>();
  for (int i = 0; i < state.videos.length; i++) {
    list.add(_buildItem(dispatch, state.videos[i], viewService.context, i));
  }
  return list;
}

Widget _buildItem(
    Dispatch dispatch, VideoInfo videoInfo, BuildContext context, int i) {
  return new Card(
    clipBehavior: Clip.antiAlias,
//    color: ColorStyle.white,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    elevation: 8,
    child: new Material(
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        splashColor: ColorStyle.main_yellow,
        borderRadius: BorderRadius.circular(10),
        onTap: () {
          onItemClick(dispatch, videoInfo);
        },
        child: new Stack(
          alignment: AlignmentDirectional.center,
          children: <Widget>[
            Align(
              alignment: AlignmentDirectional.center,
              child: FadeInImage.assetNetwork(
                placeholder: 'assets/images/ic_placeholder.png',
                image: videoInfo.imageUrl,
                width: double.infinity,
                height: double.infinity,
                fit: BoxFit.cover,
              ),
            ),
            Align(
              alignment: AlignmentDirectional.bottomEnd,
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Text(
                  '${videoInfo.title}',
                  style: TextStyle(fontSize: 16, color: Colors.green),
                  key: Key('$i'),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

///播放视频
void onItemClick(Dispatch dispatch, VideoInfo videoInfo) {
  dispatch(GridViewActionCreator.onItemClick(videoInfo));
}
