import 'package:com.sxt.flutter/generated/VideoInfo.pb.dart';
import 'package:com.sxt.flutter/pages/gridView/effect.dart';
import 'package:com.sxt.flutter/pages/gridView/reducer.dart';
import 'package:com.sxt.flutter/pages/gridView/state.dart';
import 'package:com.sxt.flutter/pages/gridView/view.dart';
import 'package:fish_redux/fish_redux.dart';

class GridViewPage extends Page<GridViewState, List<VideoInfo>> {
  GridViewPage()
      : super(
            initState: GridViewState().initState,
            effect: GridViewEffect().buildEffect(),
            reducer: GridViewReducter().buildReducer(),
            view: buildView);
}
