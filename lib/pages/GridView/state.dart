import 'package:com.sxt.flutter/generated/VideoInfo.pb.dart';
import 'package:fish_redux/fish_redux.dart';

class GridViewState implements Cloneable<GridViewState> {
  List<VideoInfo> videos = new List();

  @override
  GridViewState clone() {
    return GridViewState()..videos;
  }

  GridViewState initState(List<VideoInfo> videos) {
    final GridViewState state = GridViewState();
    state..videos = videos;
    return state;
  }
}
