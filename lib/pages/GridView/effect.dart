import 'package:com.sxt.flutter/net/TJProtocol.dart';
import 'package:com.sxt.flutter/pages/gridView/action.dart';
import 'package:com.sxt.flutter/pages/gridView/state.dart';
import 'package:com.sxt.flutter/pages/video/page.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

//事件分发
class GridViewEffect {
  Effect<GridViewState> buildEffect() {
    return combineEffects({
      Lifecycle.initState: _initState,
      GridViewAction.onRefresh: _onRefresh,
      GridViewAction.onItemClick: _onItemClick,
    });
  }

  ///初始化
  _initState(Action action, Context<GridViewState> ctx) {
    _onRefresh(action, ctx);
  }

  ///刷新数据
  _onRefresh(Action action, Context<GridViewState> ctx) async {
    var videos = await TJProtocol.instance().getVideos();
    ctx.dispatch(GridViewActionCreator.refresh(videos));
  }

  ///Item点击
  _onItemClick(Action action, Context<GridViewState> ctx) async {
    Navigator.of(ctx.context).push(MaterialPageRoute(builder: (context) {
      return VideoPage().buildPage(action.payload);
    }));
  }
}
