import 'package:com.sxt.flutter/json/VideoInfo.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoState implements Cloneable<VideoState> {
  VideoPlayerController controller;
  Duration currentPosition = Duration(milliseconds: 0);
  bool isCompleted = false,
      isShowPlayerContorller = true,
      isPortrait = true,
      isPlaying = true;
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  List<VideoInfo> videoInfos;
  VideoInfo currentVideoInfo;

  ///视频Title和 控制栏的高度
  double CONTORLLER_HEIGHT = 60;

  @override
  VideoState clone() {
    return VideoState()
      ..controller = controller
      ..scaffoldKey = scaffoldKey
      ..videoInfos = videoInfos
      ..isPortrait = isPortrait
      ..isCompleted = isCompleted
      ..currentPosition = currentPosition
      ..isShowPlayerContorller = isShowPlayerContorller;
  }

  VideoState initState(dynamic object) {
    final VideoState videoState = VideoState();
    return videoState;
  }
}
