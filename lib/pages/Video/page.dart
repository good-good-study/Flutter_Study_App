import 'package:fish_redux/fish_redux.dart';
import 'package:com.sxt.flutter/pages/video/effect.dart';
import 'package:com.sxt.flutter/pages/video/reducer.dart';
import 'package:com.sxt.flutter/pages/video/state.dart';
import 'package:com.sxt.flutter/pages/video/view.dart';

class VideoPage extends Page<VideoState, dynamic> {
  VideoPage()
      : super(
          initState: VideoState().initState,
          effect: VideoEffect().buildEffect(),
          reducer: VideoReducer().buildReducer(),
          view: buildView,
        );
}
