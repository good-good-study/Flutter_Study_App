import 'package:com.sxt.flutter/json/VideoInfo.dart';
import 'package:com.sxt.flutter/pages/video/state.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:video_player/video_player.dart';

enum VideoAction {
  refresh, //刷新数据源
  reset, //初始化播放器
  retry, //重新播放
  onRetry,
  onBack, //顶部的返回键
  seekTo, //SeekBar推拽事件
  playOrPause, //播放或者暂停
  playCompleted, //播放完成
  openDrawer, //打开侧滑菜单
  updateContorllerShown, //是否显示title和控制栏
  onScreenOrientation, //切换屏幕方向
  screenOrientation, //切换屏幕方向 刷新Widget
}

//action 定义事件分类
class VideoActionCreator {
  ///刷新数据源
  static Action refresh(List<VideoInfo> videoInfos) {
    return Action(VideoAction.refresh, payload: videoInfos);
  }

  ///重置(切换视频)
  static Action reset(VideoInfo videoInfo) {
    return Action(VideoAction.reset, payload: videoInfo);
  }

  ///重新播放
  static Action retry() {
    return Action(VideoAction.retry);
  }

  ///返回键
  static Action onBack() {
    return Action(VideoAction.onBack);
  }

  ///快进、快退
  static Action seekTo(Duration currentDuration) {
    return Action(VideoAction.seekTo, payload: currentDuration);
  }

  ///播放或暂停
  static Action playOrPause(VideoPlayerController controller) {
    return Action(VideoAction.playOrPause, payload: controller);
  }

  ///播放完成
  static Action playCompleted(bool isCompleted) {
    return Action(VideoAction.playCompleted, payload: isCompleted);
  }

  ///打开侧滑菜单
  static Action openDrawer() {
    return Action(VideoAction.openDrawer);
  }

  ///更新控制栏的状态
  static Action updateContorllerShown() {
    return Action(VideoAction.updateContorllerShown);
  }

  ///切换屏幕方向
  static Action onScreenOrientation() {
    return Action(VideoAction.onScreenOrientation);
  }

  ///切换屏幕方向后更新Widget
  static Action screenOrientation(Context<VideoState> ctx) {
    return Action(VideoAction.screenOrientation, payload: ctx);
  }
}
