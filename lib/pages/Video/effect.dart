//effect 处理来自View的意图
import 'dart:async';

import 'package:com.sxt.flutter/json/VideoInfo.dart';
import 'package:com.sxt.flutter/pages/video/action.dart';
import 'package:com.sxt.flutter/pages/video/state.dart';
import 'package:com.sxt.flutter/utils/ScreenOrientationUtil.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoEffect extends WidgetsBindingObserver {
  Effect<VideoState> buildEffect() {
    return combineEffects(<Object, Effect<VideoState>>{
      Lifecycle.initState: _initState,
      Lifecycle.dispose: _dispose,
      VideoAction.onRetry: _onRetry,
      VideoAction.onBack: _onBack,
      VideoAction.reset: _reset,
      VideoAction.onScreenOrientation: _onScreenOrientation,
    });
  }

  Context<VideoState> ctx;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    //不可视状态下,强制切换为横屏,因为横屏状态下导致ios无法解锁
    if (state == AppLifecycleState.paused) {
      ctx.state.isPortrait = false;
      _onScreenOrientation(null, ctx);
    }
  }

  ///初始化,进入视频，默认为竖屏
  void _initState(Action action, Context<VideoState> ctx) {
    this.ctx = ctx;
    WidgetsBinding.instance.addObserver(this);
    ctx.state.isPortrait = false;
    _onScreenOrientation(action, ctx).then((_) {
      print('进入视频播放界面,设置屏幕方向为竖屏');
      _requestNet(ctx);
    });
  }

  ///初始化视频列表的数据
  Future<Null> _requestNet(Context<VideoState> ctx) async {
    Future.delayed(Duration(milliseconds: 2000), () {
      ctx.state.videoInfos = new List();
      for (int i = 0; i < 20; i++) {
        var videoInfo = VideoInfo();
        String videoUrl =
            'http://video.izhaohu.com/21f560d483d441bea922dfe7631cdd3b/d20a37ab6673409fb5279361b71b90c3-b1652e9b90f6fbca44d3573623d2fe6b-ld.mp4';
        videoInfo.url = videoUrl;
        videoInfo.status = '';
        videoInfo.title = '假如生活欺骗了你';
        videoInfo.id = DateTime.now().millisecondsSinceEpoch;
        ctx.state.videoInfos.add(videoInfo);
      }
    }).then((_) {
      ctx.dispatch(VideoActionCreator.refresh(ctx.state.videoInfos));
      ctx.state.currentVideoInfo = ctx.state.videoInfos[0];
      resetController(ctx, ctx.state.currentVideoInfo); //初始化播放器
    });
  }

  void _reset(Action action, Context<VideoState> ctx) {
    resetController(ctx, action.payload);
  }

  ///初始化播放器
  void resetController(Context<VideoState> ctx, VideoInfo videoInfo) async {
    if (ctx.state.controller != null) {
      await ctx.state.controller.pause();
      ctx.dispatch(VideoActionCreator.seekTo(Duration(milliseconds: 0)));
      ctx.state.controller = null;
      print('reset');
    }
    ctx.state.controller = VideoPlayerController.network(videoInfo.url)
      ..initialize().then((_) {
        print('初始化完成,开始播放');
        _playVideo(ctx);
      });
  }

  ///播放视频
  void _playVideo(Context<VideoState> ctx) {
    if (ctx.state.controller != null &&
        ctx.state.controller.value != null &&
        ctx.state.controller.value.initialized) {
      ctx.dispatch(VideoActionCreator.playOrPause(ctx.state.controller));
    }
  }

  ///界面销毁,恢复屏幕方向，停止播放，释放资源
  void _dispose(Action action, Context<VideoState> ctx) {
    if (ctx.state.controller != null) {
      if (ctx.state.controller.value.isPlaying) {
        ctx.state.controller.pause();
      }
      ctx.state.controller.dispose();
    }
    WidgetsBinding.instance.removeObserver(this);
    ScreenOrientationUtil.setScreenorientation(false).then((value) {
      print('界面销毁,恢复屏幕方向');
    });
  }

  ///重新播放
  void _onRetry(Action action, Context<VideoState> ctx) {
    ctx.dispatch(VideoActionCreator.seekTo(Duration(milliseconds: 0)));
  }

  ///返回按钮点击事件
  ///当前为横屏时,点击返回键,切换为竖屏 ; 否则,停止播放退出界面
  void _onBack(Action action, Context<VideoState> ctx) {
    if (!ctx.state.isPortrait) {
      _onScreenOrientation(action, ctx);
    } else {
      if (ctx.state.controller != null &&
          ctx.state.controller.value != null &&
          ctx.state.controller.value.isPlaying) {
        ctx.state.controller.pause();
      }
      Navigator.of(ctx.context).pop();
    }
  }

  ///切换屏幕方向
  Future _onScreenOrientation(Action action, Context<VideoState> ctx) async {
    return ScreenOrientationUtil.setScreenorientation(ctx.state.isPortrait)
        .then((Orientation orientation) {
      //切换方向后,需要重新刷新Widget的状态
      ctx.dispatch(VideoActionCreator.screenOrientation(ctx));
    });
  }
}
