import 'package:com.sxt.flutter/pages/video/action.dart';
import 'package:com.sxt.flutter/pages/video/state.dart';
import 'package:fish_redux/fish_redux.dart';

//reducer 更新数据
class VideoReducer {
  Reducer<VideoState> buildReducer() {
    return asReducer(
      <Object, Reducer<VideoState>>{
        VideoAction.refresh: _refresh,
        VideoAction.retry: _retry,
        VideoAction.updateContorllerShown: _updateContorllerShown,
        VideoAction.playOrPause: _playOrPause,
        VideoAction.seekTo: _seekTo,
        VideoAction.playCompleted: _playCompleted,
        VideoAction.openDrawer: _openDrawer,
        VideoAction.screenOrientation: _screenOrientation,
      },
    );
  }

  ///刷新数据源
  VideoState _refresh(VideoState state, Action action) {
    VideoState newState = state.clone();
    newState.videoInfos = action.payload;
    return newState;
  }

  ///重新播放
  VideoState _retry(VideoState state, Action action) {
    VideoState newState = state.clone();
    if (newState.controller != null && newState.controller.value != null) {
      newState.controller.seekTo(Duration(milliseconds: 0));
      newState.isCompleted = false;
    }
    return newState;
  }

  ///播放或暂停
  VideoState _playOrPause(VideoState state, Action action) {
    VideoState newState = state.clone();
    newState.controller = action.payload;
    if (newState.controller != null &&
        newState.controller.value != null &&
        newState.controller.value.initialized) {
      if (newState.controller.value.isPlaying) {
        newState.controller.pause();
        newState.isPlaying = false;
      } else {
        newState.controller.play();
        newState.isPlaying = true;
      }
      newState.currentPosition = newState.controller.value.position;
    }
    return newState;
  }

  ///快进,快退
  VideoState _seekTo(VideoState state, Action action) {
    VideoState newState = state.clone();
    if (newState.controller != null) {
      newState.isCompleted = false;
      newState.controller.seekTo(action.payload);
      newState.controller.play();
      newState.currentPosition = action.payload;
    }
    return newState;
  }

  ///播放完成
  VideoState _playCompleted(VideoState state, Action action) {
    VideoState newState = state.clone();
    newState.isCompleted = true;
    if (newState.controller != null) {
      var duration = newState.controller.value.duration;
      if (newState.currentPosition < duration) {
        newState.controller.seekTo(duration);
      }
      newState.currentPosition = duration;
    }
    return newState;
  }

  ///控制Title和控制栏是否显示
  VideoState _updateContorllerShown(VideoState state, Action action) {
    VideoState newState = state.clone();
    newState.isShowPlayerContorller = !state.isShowPlayerContorller;
    if (newState.controller != null &&
        newState.controller.value != null &&
        newState.controller.value.initialized) {
      newState.currentPosition = newState.controller.value.position;
    }
    return newState;
  }

  ///打开侧滑菜单
  VideoState _openDrawer(VideoState state, Action action) {
    var newState = state.clone();
    newState.scaffoldKey.currentState.openEndDrawer();
    return newState;
  }

  ///横竖屏切换后,更新状态
  VideoState _screenOrientation(VideoState state, Action action) {
    Context<VideoState> ctx = action.payload;
    VideoState newState = state.clone();
    newState.controller = ctx.state.controller;
    newState.isPortrait = ctx.state.isPortrait;
    return newState;
  }
}
