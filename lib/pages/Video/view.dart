import 'package:com.sxt.flutter/item/Item_video_course_content.dart';
import 'package:com.sxt.flutter/item/Item_video_course_title.dart';
import 'package:com.sxt.flutter/item/Item_video_progress_indicator.dart';
import 'package:com.sxt.flutter/item/Item_video_retry.dart';
import 'package:com.sxt.flutter/item/Item_video_title.dart';
import 'package:com.sxt.flutter/item/Item_vido_drawer_content.dart';
import 'package:com.sxt.flutter/item/Item_vido_drawer_title.dart';
import 'package:com.sxt.flutter/json/VideoInfo.dart';
import 'package:com.sxt.flutter/pages/video/action.dart';
import 'package:com.sxt.flutter/pages/video/state.dart';
import 'package:com.sxt.flutter/values/colors.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';

Widget buildView(VideoState state, Dispatch dispatch, ViewService viewService) {
  return new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: new Scaffold(
      key: state.scaffoldKey,
      endDrawer: _buildDrawer(state, dispatch, viewService),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
          child: OrientationBuilder(builder: (context, orientation) {
            state.isPortrait = orientation == Orientation.portrait;
            return _buildVideoWidgets(state, dispatch, viewService);
          }),
          value: SystemUiOverlayStyle(statusBarColor: ColorStyle.transparent)),
    ),
  );
}

///视频播放
Widget _buildVideoWidgets(
    VideoState state, Dispatch dispatch, ViewService viewService) {
  List<Widget> _listWidget = new List();
  _listWidget.add(Container(
      color: Color(0xFFAEAEAE),
      width: double.infinity,
      height: state.isPortrait
          ? MediaQuery.of(viewService.context).size.height / 3
          : MediaQuery.of(viewService.context).size.height,
      child: Stack(
        children: <Widget>[
          _buildPlayerView(state, dispatch), //videoPlayer
          _buildPlayerTitle(
              state,
              dispatch,
              state.currentVideoInfo == null
                  ? ''
                  : state.currentVideoInfo.title), //Title
          _buildPlayerControlView(state, dispatch), //SeekBar
          _buildPlayerRetry(state, dispatch), // retry
          _buildLoading(state) //Loading
        ],
      )));
  //章节列表
  if (state.isPortrait) {
    _listWidget.add(SizedBox(
      height: 8,
    ));
    _listWidget.add(VideoCourseTitle(
      title: state.currentVideoInfo == null ? '' : state.currentVideoInfo.title,
    ));
    _listWidget.add(_buildCourseList(state, dispatch));
  }
  return state.isPortrait
      ? Column(
          children: _listWidget,
        )
      : ListView(
          padding: EdgeInsets.all(0),
          children: _listWidget,
        );
}

Align _buildLoading(VideoState state) {
  return Align(
    alignment: AlignmentDirectional.center,
    child: state.controller != null &&
            state.controller.value != null &&
            state.controller.value.initialized
        ? Container(
            width: 0,
            height: 0,
          )
        : CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(Colors.deepPurple),
          ),
  );
}

///视频Widget
Align _buildPlayerView(VideoState state, Dispatch dispatch) {
  return Align(
      child: Container(
    height: double.infinity,
    width: double.infinity,
    color: ColorStyle.black,
    child: InkWell(
      onTap: () {
        dispatch(VideoActionCreator.updateContorllerShown());
      },
      child: state.controller != null && state.controller.value != null
          ? AspectRatio(
              aspectRatio: state.controller.value.aspectRatio,
              child: VideoPlayer(state.controller),
            )
          : Container(),
    ),
  ));
}

///视频的Title
Widget _buildPlayerTitle(
    VideoState state, Dispatch dispatch, String videoTitle) {
  var widget = VideoTitle(
    title: videoTitle,
    onBackClick: () => onBackClick(state, dispatch),
    onRightClick: () => onRightClick(state, dispatch),
    isShowRight: !state.isPortrait,
  );
  return Transform.translate(
    offset:
        Offset(0, state.isShowPlayerContorller ? 0 : -state.CONTORLLER_HEIGHT),
    child: widget,
  );
}

///播放进度控制栏
Widget _buildPlayerControlView(VideoState state, Dispatch dispatch) {
  print('View : state.controller : ${state.controller}');
  var align = Align(
    alignment: AlignmentDirectional.bottomCenter,
    child: Container(
      height: state.isShowPlayerContorller ? state.CONTORLLER_HEIGHT : 0,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [ColorStyle.transparent, ColorStyle.black],
            begin: AlignmentDirectional.topCenter,
            end: AlignmentDirectional.bottomCenter),
      ),
      child: state.controller != null &&
              state.controller.value != null &&
              state.controller.value.initialized
          ? VideoProgressBar(
              textColor: ColorStyle.white,
              activeColor: ColorStyle.white,
              videoController: state.controller,
              isPlaying: state.isPlaying,
              isPortrait: state.isPortrait,
              isCompleted: state.isCompleted,
              currentPosition: state.currentPosition == null
                  ? Duration(milliseconds: 0)
                  : state.currentPosition,
              progressListener: OnChangedListener(state, dispatch),
            )
          : Container(),
    ),
  );

  return Transform.translate(
    offset:
        Offset(0, state.isShowPlayerContorller ? 0 : state.CONTORLLER_HEIGHT),
    child: align,
  );
}

///重新播放
Widget _buildPlayerRetry(VideoState state, Dispatch dispatch) {
  var widget = VideoRetry(
    isCompleted: state.isCompleted,
    onRetry: () {
      dispatch(VideoActionCreator.retry());
    },
  );
  return widget;
}

///章节列表,仅在竖屏状态下显示
Widget _buildCourseList(VideoState state, Dispatch dispatch) {
  return Expanded(
      child: ListView.builder(
    padding: EdgeInsets.all(0),
    itemCount: state.videoInfos == null ? 0 : state.videoInfos.length,
    physics: BouncingScrollPhysics(),
    itemBuilder: (context, index) {
      return VideoCourseContent(
        index: index,
        title: state.videoInfos[index].title,
        onItemClick: (index) => onItemClick(state.videoInfos[index], dispatch),
      );
    },
  ));
}

///右侧的侧滑菜单,仅在横屏状态下显示
Widget _buildDrawer(
    VideoState state, Dispatch dispatch, ViewService viewService) {
  return state.isPortrait
      ? null
      : Column(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(viewService.context).padding.top,
            ),
            VideoDrawerTitle(),
            Expanded(
                child: Container(
              alignment: AlignmentDirectional.topStart,
              width: MediaQuery.of(viewService.context).size.width * 0.38,
              color: ColorStyle.alpha_76,
              child: ListView.builder(
                  padding: EdgeInsets.all(0),
                  physics: AlwaysScrollableScrollPhysics(),
                  itemCount:
                      state.videoInfos == null ? 0 : state.videoInfos.length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: <Widget>[
                        Opacity(
                          opacity: 0.5,
                          child: Divider(
                            height: 1,
                            color: index == 0
                                ? ColorStyle.transparent
                                : ColorStyle.white,
                          ),
                        ),
                        VideoDrawerContent(
                          index: index,
                          data: state.videoInfos[index].title,
                          onDrawerItemClick: (index) => onDrawerItemClick(
                              state.videoInfos[index], dispatch),
                        )
                      ],
                    );
                  }),
            ))
          ],
        );
}

///章节列表点击事件
void onItemClick(VideoInfo videoInfo, Dispatch dispatch) {
  dispatch(VideoActionCreator.reset(videoInfo));
}

///返回按钮点击事件
///当前为横屏时,点击返回键,切换为竖屏 ; 否则,停止播放退出界面
void onBackClick(VideoState state, Dispatch dispatch) {
  dispatch(VideoActionCreator.onBack());
}

///章节点击 打开右侧的侧滑菜单
void onRightClick(VideoState state, Dispatch dispatch) {
  dispatch(VideoActionCreator.openDrawer());
}

///右侧菜单列表点击事件
void onDrawerItemClick(VideoInfo videoInfo, Dispatch dispatch) {
  dispatch(VideoActionCreator.reset(videoInfo));
}

class OnChangedListener implements OnVideoStateChangedListener {
  final Dispatch dispatch;
  final VideoState state;

  OnChangedListener(this.state, this.dispatch);

  @override
  void onPlayCompleted() {
    dispatch(VideoActionCreator.playCompleted(true));
  }

  @override
  onPlayOrPauseClick() {
    dispatch(VideoActionCreator.playOrPause(state.controller));
  }

  @override
  onProgressChanged(Duration currentDuration) {
    dispatch(VideoActionCreator.seekTo(currentDuration));
  }

  @override
  onSwitchOrientationClick() {
    dispatch(VideoActionCreator.onScreenOrientation());
  }
}
