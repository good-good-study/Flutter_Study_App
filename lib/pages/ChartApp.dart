import 'dart:async';

import 'package:com.sxt.flutter/item/Title.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chart/flutter_chart.dart';

class ChartApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ChartAppStateFul(),
    );
  }
}

class ChartAppStateFul extends StatefulWidget {
  final Duration _duration = const Duration(milliseconds: 1000);

  @override
  State<StatefulWidget> createState() => ChartAppState();
}

class ChartAppState extends State<ChartAppStateFul>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  CurvedAnimation curve;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this, duration: widget._duration);
    curve = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    Tween<int>(begin: 0, end: 1).animate(_controller)
      ..addStatusListener((AnimationStatus staus) {
        if (_controller.isCompleted) {
          print('动画结束');
        }
      });
    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light
          .copyWith(statusBarColor: Colors.transparent),
      child: Scaffold(
        key: _scaffoldKey,
        appBar: TitleBar(
          isShowBack: false,
          backgroundColor: Colors.deepPurple,
          title: 'Animation',
          titleStyle: TextStyle(
              color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
        ),
        body: CustomScrollView(
          key: Key('chart_scrollView_list'),
          physics: BouncingScrollPhysics(),
          slivers: <Widget>[
            SliverAppBar(
              flexibleSpace: SingleChildScrollView(
                key: Key('chart_scrollView_horizontal'),
                padding: EdgeInsets.only(left: 8, right: 8, top: 16, bottom: 8),
                scrollDirection: Axis.horizontal,
                physics: BouncingScrollPhysics(),
                child: Row(
                  children: _buildTopList(context),
                ),
              ),
              expandedHeight: 50,
//                      pinned: true,
              backgroundColor: Colors.white,
            ),
            SliverList(
                delegate: SliverChildListDelegate([
              StreamBuilder<List>(
                  initialData: list,
                  stream: stream,
                  builder: (context, result) {
                    var data = result.data;
                    print('result -> ${data.toString()}');
                    return Container();
                  }),
              SizedBox(
                height: 8,
              ),
              _buildChartLine(),
              SizedBox(
                height: 8,
              ),
              _buildChartBar(),
              SizedBox(
                height: 8,
              ),
              SizedBox(
                height: 8,
              ),
              _buildChartPie(),
            ]))
          ],
        ),
      ),
    );
  }

  List<Widget> _buildTopList(BuildContext context) {
    List<String> titles = [
      'Google',
      'Play Store',
      'YouTube',
      'Google Pay',
      'Google Map',
      'Gmail',
      'Pictures',
      'Files'
    ];
    List<Widget> _list = new List();
    for (int i = 0; i < titles.length; i++) {
      Widget widget = GestureDetector(
        onTap: () {
          _dealwithStream();
        },
        child: FadeTransition(
            opacity: curve,
            child: Container(
                margin: EdgeInsets.only(left: 8, right: 8),
                padding:
                    EdgeInsets.only(left: 16, top: 8, bottom: 8, right: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(32)),
                    color: Colors.deepPurple),
                child: Text(
                  titles[i],
                  style: TextStyle(color: Colors.white),
                ))),
      );
      _list.add(widget);
    }

    return _list;
  }

  Widget _buildChartLine() {
    var chartLine = ChartLine(
      chartBeans: [
        ChartBean(x: '12-01', y: 30),
        ChartBean(x: '12-02', y: 88),
        ChartBean(x: '12-06', y: 20),
        ChartBean(x: '12-06', y: 67),
        ChartBean(x: '12-06', y: 10),
        ChartBean(x: '12-06', y: 40),
        ChartBean(x: '12-06', y: 10),
      ],
      size: Size(MediaQuery.of(context).size.width,
          MediaQuery.of(context).size.height / 5 * 1.6),
      isCurve: true,
      lineWidth: 6,
      lineColor: Colors.deepPurple,
      fontColor: Colors.white,
      xyColor: Colors.white,
      shaderColors: [Colors.yellow, Colors.lightGreenAccent, Colors.blue],
      fontSize: 12,
      yNum: 8,
      backgroundColor: Colors.black,
      isAnimation: true,
      duration: Duration(milliseconds: 3000),
    );

    return chartLine;
  }

  ChartBar _buildChartBar() {
    return ChartBar(
      chartBeans: [
        ChartBean(x: '12-01', y: 30, color: Colors.red),
        ChartBean(x: '12-02', y: 100, color: Colors.yellow),
        ChartBean(x: '12-03', y: 70, color: Colors.green),
        ChartBean(x: '12-04', y: 70, color: Colors.blue),
        ChartBean(x: '12-05', y: 30, color: Colors.deepPurple),
        ChartBean(x: '12-06', y: 90, color: Colors.deepOrange),
        ChartBean(x: '12-07', y: 50, color: Colors.greenAccent)
      ],
      size: Size(MediaQuery.of(context).size.width,
          MediaQuery.of(context).size.height / 5 * 1.8),
      rectColor: Colors.deepPurple,
      backgroundColor: Colors.black,
      isShowX: true,
      fontColor: Colors.white,
    );
  }

  ChartPie _buildChartPie() {
    return ChartPie(
      chartBeans: [
        ChartPieBean(type: '话费', value: 30, color: Colors.blueGrey),
        ChartPieBean(type: '零食', value: 120, color: Colors.deepPurple),
        ChartPieBean(type: '衣服', value: 60, color: Colors.green),
        ChartPieBean(type: '早餐', value: 60, color: Colors.blue),
        ChartPieBean(type: '水果', value: 30, color: Colors.red),
      ],
      size: Size(
          MediaQuery.of(context).size.width, MediaQuery.of(context).size.width),
      backgroundColor: Colors.black,
      R: MediaQuery.of(context).size.width / 3,
      centerR: 6,
      duration: Duration(milliseconds: 3000),
      isCycle: true,
      centerColor: Colors.white,
      fontColor: Colors.white,
    );
  }

  var list = [1, 2, 3, 4];
  var stream;
  var streamController = StreamController<List<int>>.broadcast();

  void _dealwithStream() {
    try {
      if (stream == null)
        stream = streamController.stream.map((data) {
          list.addAll(data);
          print('StreamController -> addData : $data  ; result : $list');
          return list;
        });

      streamController.add([8]);

      setState(() {});

      print('StreamController -> add , list : $list');
      var streamSingle = Stream.fromFuture(Future(() {
        return list;
      }));
      streamSingle.listen((data) {
        print('onData  -> $data');
      }, onError: () {
        print('onError');
      }, onDone: () {
        print('onDone');
      });
    } catch (e) {
      print('Exception -> ${e.toString()}');
    }
  }
}
