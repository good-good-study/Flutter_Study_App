import 'package:com.sxt.flutter/generated/Tab.pb.dart' as grpc;
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

class TabBarState implements Cloneable<TabBarState> {
  List<grpc.Tab> tabs = new List();
  TabController tabController;

  @override
  TabBarState clone() {
    return TabBarState()
      ..tabs
      ..tabController;
  }

  TabBarState initState(dynamic object) {
    final TabBarState videoState = TabBarState();
    return videoState;
  }
}
