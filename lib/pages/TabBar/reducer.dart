import 'package:com.sxt.flutter/pages/tabBar/action.dart';
import 'package:com.sxt.flutter/pages/tabBar/state.dart';
import 'package:fish_redux/fish_redux.dart';

class TabBarReducer {
  Reducer<TabBarState> buildReducer() {
    return asReducer({TabBarAction.refresh: _refresh});
  }

  ///刷新数据源
  TabBarState _refresh(TabBarState state, Action action) {
    TabBarState newState = state.clone();
    newState.tabs = action.payload == null ? new List() : action.payload;
    return newState;
  }
}
