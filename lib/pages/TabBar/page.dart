import 'package:com.sxt.flutter/pages/tabBar/effect.dart';
import 'package:com.sxt.flutter/pages/tabBar/reducer.dart';
import 'package:com.sxt.flutter/pages/tabBar/state.dart';
import 'package:com.sxt.flutter/pages/tabBar/view.dart';
import 'package:fish_redux/fish_redux.dart';

class TabBarPage extends Page<TabBarState, dynamic> {
  TabBarPage()
      : super(
            initState: TabBarState().initState,
            effect: TabBarEffect().buildEffect(),
            reducer: TabBarReducer().buildReducer(),
            view: buildView);
}
