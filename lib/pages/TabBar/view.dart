import 'dart:async';
import 'dart:ui';

import 'package:com.sxt.flutter/generated/Tab.pb.dart' as grpc;
import 'package:com.sxt.flutter/item/Title.dart';
import 'package:com.sxt.flutter/item/item_no_data_layout.dart';
import 'package:com.sxt.flutter/pages/tabBar/action.dart';
import 'package:com.sxt.flutter/pages/tabBar/state.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Widget buildView(
    TabBarState state, Dispatch dispatch, ViewService viewService) {
  return MaterialApp(
    debugShowCheckedModeBanner: false,
    home: TabBarApp(state, dispatch, viewService),
  );
}

class TabBarApp extends StatefulWidget {
  TabBarState state;
  Dispatch dispatch;
  ViewService viewService;

  TabBarApp(this.state, this.dispatch, this.viewService);

  @override
  State<StatefulWidget> createState() => new TabAppBarState();
}

class TabAppBarState extends State<TabBarApp>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  TabController tabController;
  int _index = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      getTabs();
    }
  }

  @override
  void dispose() {
    if (tabController != null) {
      tabController.dispose();
    }
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    tabController =
        new TabController(length: widget.state.tabs.length, vsync: this);
    tabController.addListener(() {
      _index = tabController.index;
    });
    tabController.animateTo(_index);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light
            .copyWith(statusBarColor: Colors.transparent),
        child: widget.state.tabs == null || widget.state.tabs.length == 0
            ? _buildEmptyView(context)
            : _buildDefaultTabController(context),
      ),
    );
  }

  ///无数据界面
  Widget _buildEmptyView(BuildContext context) {
    return Scaffold(
      body: WidgetNoData(),
    );
  }

  ///TabBar
  DefaultTabController _buildDefaultTabController(BuildContext context) {
    return new DefaultTabController(
        initialIndex: _index,
        length: widget.state.tabs.length,
        child: new Scaffold(
          appBar: new TitleBar(
              appBarHeight: 80,
              backgroundColor: Colors.deepOrange,
              rootContainer: new TabBar(
                key: Key('tabBar'),
                controller: tabController,
                indicatorSize: TabBarIndicatorSize.label,
                indicatorColor: Colors.white,
                isScrollable: true,
                tabs: widget.state.tabs.map((grpc.Tab tab) {
                  return new Tab(
                    key: Key('${tab.title}'),
                    text: tab.title,
                    icon: new Icon(
                      Icons.android,
                      size: 23,
                    ),
                  );
                }).toList(),
              )),
          body: new TabBarView(
            key: Key('tabBarView'),
            controller: tabController,
            children: widget.state.tabs.map((grpc.Tab tab) {
              return RefreshIndicator(
                color: Colors.deepOrange,
                onRefresh: _refresh,
                child: new ListView(
                  key: Key('listView${widget.state.tabs.indexOf(tab)}'),
                  padding: EdgeInsets.all(0),
                  physics: BouncingScrollPhysics(),
                  children: tab.items.map((grpc.Item item) {
                    return _buildItems(item, context);
                  }).toList(),
                ),
              );
            }).toList(),
          ),
        ));
  }

  ///内容填充
  Widget _buildItems(grpc.Item item, BuildContext context) {
    return InkWell(
      child: Container(
        height: MediaQuery.of(context).size.height / 3,
        width: MediaQuery.of(context).size.width,
        child: Hero(
            tag: item.url,
            child: Padding(
              padding: EdgeInsets.only(bottom: 2),
              child: FadeInImage.assetNetwork(
                placeholder: 'assets/images/ic_placeholder.png',
                image: item.url,
                fit: BoxFit.cover,
              ),
            )),
      ),
      onTap: () {
        onItemClick(item);
      },
    );
  }

  ///获取Tab数据
  void getTabs() {
    widget.dispatch(TabBarActionCreator.onRefresh());
  }

  ///刷新数据
  Future<void> _refresh() async {
    return Future.delayed(Duration(seconds: 2)).then((_) {
      getTabs();
    });
  }

  void onItemClick(grpc.Item item) {
    widget.dispatch(TabBarActionCreator.onItemClick(item));
  }
}
