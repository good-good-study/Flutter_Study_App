import 'package:com.sxt.flutter/generated/Tab.pb.dart';
import 'package:fish_redux/fish_redux.dart';

//定义Action类型
enum TabBarAction {
  onRefresh, //准备刷新数据
  refresh, //刷新数据
  onItemClick, //图片点击
}

//定义Action事件
class TabBarActionCreator {
  ///当刷新数据时
  static Action onRefresh() {
    return Action(TabBarAction.onRefresh);
  }

  ///刷新数据
  static Action refresh(List<Tab> tabs) {
    return Action(TabBarAction.refresh, payload: tabs);
  }

  ///Item点击事件
  static Action onItemClick(Item item) {
    return Action(TabBarAction.onItemClick, payload: item);
  }
}
