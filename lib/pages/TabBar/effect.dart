import 'package:com.sxt.flutter/net/TJProtocol.dart';
import 'package:com.sxt.flutter/pages/tabBar/action.dart';
import 'package:com.sxt.flutter/pages/tabBar/state.dart';
import 'package:com.sxt.flutter/utils/Constants.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

//事件分发
class TabBarEffect {
  Effect<TabBarState> buildEffect() {
    return combineEffects({
      Lifecycle.initState: _initState,
      TabBarAction.onRefresh: _onRefresh,
      TabBarAction.onItemClick: _onItemCLick
    });
  }

  ///初始化
  _initState(Action action, Context<TabBarState> ctx) {
    _onRefresh(action, ctx);
  }

  ///刷新数据
  _onRefresh(Action action, Context<TabBarState> ctx) async {
    var tabs = await TJProtocol.instance().getTabs(0);
    ctx.dispatch(TabBarActionCreator.refresh(tabs));
  }

  _onItemCLick(Action action, Context<TabBarState> ctx) {
    String url = action.payload.url;
    Navigator.of(ctx.context).push(new MaterialPageRoute(builder: (context) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Hero(
                tag: url,
                child: FadeInImage.assetNetwork(
                  placeholder: 'assets/images/ic_placeholder.png',
                  image: url,
                  fit: BoxFit.cover,
                )),
          ],
        ),
      );
    }));
  }
}
