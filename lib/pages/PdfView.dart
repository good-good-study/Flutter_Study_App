import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:com.sxt.flutter/item/loading.dart';
import 'package:com.sxt.flutter/item/title.dart';
import 'package:com.sxt.flutter/values/colors.dart';
import 'package:path_provider/path_provider.dart';

class PdfView extends StatefulWidget {
  final String url;
  final isFromNetWork;

  const PdfView(
      {Key key,
      this.url = 'http://africau.edu/images/default/sample.pdf',
      this.isFromNetWork = true})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => PdfViewState();
}

class PdfViewState extends State<PdfView> {
  var _loading;
  BuildContext _context;
  String path;

  @override
  void initState() {
    super.initState();
    if (widget.isFromNetWork) {
      _download();
    } else {
      path = widget.url;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      appBar: TitleBar(
        backgroundColor: ColorStyle.white,
        title: 'PDF',
        onBackClick: () {
          Navigator.of(_context).pop();
        },
      ),
      body: path == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : PDFViewerScaffold(
              path: path,
            ),
    );
  }

  ///刷新数据
  Future<Null> _download() async {
    _showLoading(context);
    createFileOfPdfUrl(widget.isFromNetWork).then((file) {
      _dismiss();
      path = file.path;
      setState(() {});
      print('下载完成的文件路径 ： path -> ${file.path}');
    });
  }

  Future<File> createFileOfPdfUrl(bool isFromNetWork) async {
    ///data/data/package/app_flutter (app包名下的app_flutter文件夹)
    String dir = (await getApplicationDocumentsDirectory()).path;
    String fileName;
    File file;
    if (isFromNetWork) {
      final filename = widget.url.substring(widget.url.lastIndexOf("/") + 1);
      var request = await HttpClient().getUrl(Uri.parse(widget.url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);

      file = new File('$dir/$filename');
      await file.writeAsBytes(bytes);
    } else {
      fileName = 'test.pdf';
      var bytes = await rootBundle.load('assets/file/test.pdf');

      ///data/data/package/app_flutter (app包名下的app_flutter文件夹)
      String dir = (await getApplicationDocumentsDirectory()).path;
      file = new File('$dir/$fileName');
      writeToFile(bytes, '$dir/$fileName');
    }

    return file;
  }

  Future<void> writeToFile(ByteData data, String path) {
    final buffer = data.buffer;
    return new File(path).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  void _showLoading(BuildContext context) {
    if (_loading == null) {
      _loading = new Loading();
    }
    _loading.showLoading(context, false);
  }

  void _dismiss() {
    if (_loading != null) {
      _loading.dismiss();
    }
  }
}
