import 'package:connectivity/connectivity.dart';
import 'package:com.sxt.flutter/pages/Home.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'net/FlutterService.dart';
import 'package:jpush_flutter/jpush_flutter.dart';

const String channel_method = "default_channel_method";
MethodChannel channel = MethodChannel(channel_method);

void main() async {
  FlutterService().start(); //启动grpc服务
  initNetConnectState(); //监听设备网络连接的状态
  initJpush();
  runApp(HomeApp());
}

JPush jPush = JPush();
void initJpush() async {
//  var rid = await jPush.getRegistrationID();

  jPush.addEventHandler(
      onReceiveNotification: (Map<String, dynamic> message) async {
    parseMessage(message);
  }, onOpenNotification: (Map<String, dynamic> message) async {
    parseMessage(message);
  }, onReceiveMessage: (Map<String, dynamic> message) async {
    parseMessage(message);
  });

  jPush.setup(
      appKey: '95bf551bd4df957132282eee',
      channel: 'developer-default',
      production: false,
      debug: false);
//
//  jPush.applyPushAuthority(
//      new NotificationSettingsIOS(sound: true, alert: true, badge: true));
}

void parseMessage(Map<String, dynamic> message) {
  print('Jpush -> message : $message');
  if (message != null) {
    var msg = message['alert'];
    var map = message['extras'];
    if (map != null) {
      var bigText = map['cn.jpush.android.BIG_TEXT'];
      if (bigText != null) {
        msg = bigText;
      }
    }
    if (msg == null) {
      msg = message['message'];
    }
    print('Jpush -> message : $msg');
    channel.invokeMethod('playNotifyMusic', msg);
  }
}

///监听设备网络连接的状态
void initNetConnectState() async {
  ///通过订阅的方式监听设备网络连接的情况 ，但对于ios设备这种方式只有在网络连接方式变化的时候才会响应
  Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
    print('onConnectivityChanged result - > ${result.toString()}');
  });

  ///通过Future的方式异步获取当前设备的网络连接方式 ，android和ios都适用
  var result = await Connectivity().checkConnectivity();
  print('Future<ConnectivityResult> result - >${result.toString()} ');
}
