import 'package:flutter/material.dart';
import 'package:com.sxt.flutter/utils/DateFormatUtil.dart';
import 'package:com.sxt.flutter/values/colors.dart';
import 'package:video_player/video_player.dart';

class VideoProgressBar extends StatefulWidget {
  ///时间文字颜色 ,进度条默认颜色 ,当前进度的颜色
  final Color textColor, inactiveColor, activeColor;

  ///播放和暂停按钮的图片资源
  final String playIconUrl, pauseIconUrl;

  ///设置屏幕方向的按钮图片资源
  final String fullScreenIconUrl, unFullScreenIconUrl;
  final bool isPlaying, isPortrait, isCompleted;
  final Duration currentPosition;
  final OnVideoStateChangedListener progressListener;
  final VideoPlayerController videoController;

  const VideoProgressBar({
    Key key,
    @required this.videoController,
    this.inactiveColor = ColorStyle.gray_889397,
    this.activeColor = ColorStyle.white,
    this.textColor = ColorStyle.white,
    this.playIconUrl = 'assets/images/icon_video_play.png',
    this.pauseIconUrl = 'assets/images/icon_video_pause.png',
    this.fullScreenIconUrl = 'assets/images/icon_fullscreen.png',
    this.unFullScreenIconUrl = 'assets/images/icon_unfullscreen.png',
    this.isPlaying = true,
    this.isPortrait = true,
    this.isCompleted = false,
    this.currentPosition,
    this.progressListener,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _VideoProgressIndicatorState();
  }
}

class _VideoProgressIndicatorState extends State<VideoProgressBar> {
  Duration _position = Duration(milliseconds: 0);
  Duration _duration = Duration(milliseconds: 0);
  double _value = 0.0, _maxValue = 0.0;
  VoidCallback listener;
  bool isDraging = false;

  @override
  void initState() {
    super.initState();
    if (widget.videoController == null) return;
    Duration duration = widget.videoController.value.duration;
    _duration = duration == null ? Duration(milliseconds: 0) : duration;
    _maxValue = _duration.inMilliseconds.toDouble();
    _value = widget.currentPosition != null
        ? widget.currentPosition.inMilliseconds.toDouble()
        : _position.inMilliseconds.toDouble();
    print('初始化进度 _value : $_value _maxValue : $_maxValue');
    widget.videoController.position.then((position) {
      setState(() {
        _value = position.inMilliseconds.toDouble();
        if (listener == null && !widget.isCompleted) {
          listener = () {
            if (widget.isCompleted || isDraging) return;
            setState(() {
              //更新进度条
              _position = widget.videoController.value.position;
              _value = _position.inMilliseconds.toDouble();
              if (widget.progressListener != null &&
                  _position >= _duration &&
                  _duration.inMilliseconds > 0) {
                _value = _maxValue;
                listener = null;
                widget.progressListener.onPlayCompleted();
                print('播放完成');
              }
            });
          };
          widget.videoController.addListener(listener);
        }
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    if (listener != null) {
      widget.videoController.removeListener(listener);
      listener = null;
      print('释放: listener: ${listener.toString()}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return _buildPlayerControlView();
  }

  ///播放进度控制栏
  Widget _buildPlayerControlView() {
    return Container(
      alignment: AlignmentDirectional.bottomCenter,
      padding: EdgeInsets.all(15),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ///播放&暂停按钮
          InkWell(
            onTap: () {
              if (widget.progressListener != null) {
                widget.progressListener.onPlayOrPauseClick();
              }
            },
            child: Image.asset(
              widget.isPlaying ? widget.pauseIconUrl : widget.playIconUrl,
              width: 24,
              height: 24,
            ),
          ),

          ///当前视频播放进度
          Padding(
            padding: EdgeInsets.only(
              left: 15,
            ),
            child: Text(
              DateFormatUtil.getTimeFromDuration(
                  Duration(milliseconds: _value.toInt()),
                  flag: DateFormatUtil.DATE_HH_MM),
              style: TextStyle(color: widget.textColor, fontSize: 10),
            ),
          ),

          ///播放进度条
          Expanded(
            child: SizedBox(
              height: 14,
              child: Slider(
                value: _value > _maxValue ? _maxValue : _value,
                min: 0.0,
                inactiveColor: widget.inactiveColor,
                activeColor: widget.activeColor,
                max: _maxValue,
                onChanged: (double value) {
                  if (this.mounted)
                    setState(() {
                      _value = value;
                      if (!isDraging) isDraging = true;
                      print(
                          'onChanged :isDraging $isDraging , _value : $_value , maxValue : $_maxValue');
                    });
                },
                onChangeEnd: (value) {
                  _value = value;
                  print('onChangeEnd ${(value / _maxValue) * 100}');
                  if (isDraging) isDraging = false;
                  if (_value >= _maxValue) {
                    if (widget.progressListener != null &&
                        widget.currentPosition != null &&
                        widget.currentPosition.inMilliseconds > 0) {
                      widget.progressListener.onPlayCompleted();
                    }
                    return;
                  }
                  if (widget.progressListener != null) {
                    widget.progressListener.onProgressChanged(
                        Duration(milliseconds: _value.toInt()));
                  }
                },
              ),
            ),
          ),

          ///当前播放总时长
          Padding(
            child: Text(
              DateFormatUtil.getTimeFromDuration(_duration,
                  flag: DateFormatUtil.DATE_HH_MM),
              style: TextStyle(color: widget.textColor, fontSize: 10),
            ),
            padding: EdgeInsets.only(right: 15),
          ),

          ///切换屏幕方向的按钮
          InkWell(
            onTap: () {
              if (widget.progressListener != null) {
                widget.progressListener.onSwitchOrientationClick();
              }
            },
            child: Image.asset(
              widget.isPortrait
                  ? widget.fullScreenIconUrl
                  : widget.unFullScreenIconUrl,
              width: 24,
              height: 24,
            ),
          )
        ],
      ),
    );
  }
}

abstract class OnVideoStateChangedListener {
  ///当播放或暂停按钮点击时
  onPlayOrPauseClick();

  ///当进度条拖动后
  onProgressChanged(Duration currentDuration);

  ///当全屏切换按钮点击时
  onSwitchOrientationClick();

  void onPlayCompleted();
}
