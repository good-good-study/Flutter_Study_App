import 'package:flutter/material.dart';

class BottomTabsWidget extends StatefulWidget {
  final BottomNavigationBarType type;
  final List<String> tabIconTitles;
  final List<IconData> tabIconData;
  final List<Color> tabIconColors;
  final TextStyle tabSelectStyle;
  final TextStyle tabUnselectStyle;

  final Function(int index) onPressed;

  const BottomTabsWidget({
    Key key,
    @required this.tabIconTitles,
    @required this.tabIconData,
    @required this.tabIconColors,
    this.tabSelectStyle,
    this.tabUnselectStyle,
    this.type = BottomNavigationBarType.fixed,
    this.onPressed,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => BottomTabState();
}

class BottomTabState extends State<BottomTabsWidget> {
  int tab_index = 0;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: _buildNavigationBarItems(),
      currentIndex: tab_index,
      type: widget.type,
      onTap: (index) {
        if (index != tab_index) {
          setState(() {
            tab_index = index;
            if (widget.onPressed != null) widget.onPressed(index);
          });
        }
      },
    );
  }

  ///List<BottomNavigationBarItem>
  List<BottomNavigationBarItem> _buildNavigationBarItems() {
    final list = new List<BottomNavigationBarItem>();

    for (String title in widget.tabIconTitles) {
      int currentIndex = widget.tabIconTitles.indexOf(title, 0);
      Icon icon;
      Icon activeIcon;

      icon = new Icon(
        widget.tabIconData[currentIndex],
        size: 20,
        color: widget.type == BottomNavigationBarType.shifting
            ? null
            : widget.tabIconColors[currentIndex].withOpacity(0.6),
      );
      activeIcon = new Icon(
        widget.tabIconData[currentIndex],
        size: 20,
        color: widget.type == BottomNavigationBarType.shifting
            ? null
            : widget.tabIconColors[currentIndex],
      );

      var item = _buildNavigationItem(
          new Text(
            title,
            key: Key(title),
            style: tab_index == currentIndex
                ? widget.tabSelectStyle != null
                    ? widget.tabSelectStyle
                    : TextStyle(
                        fontSize: 12,
                        color: widget.type == BottomNavigationBarType.shifting
                            ? Colors.white
                            : Colors.black)
                : widget.tabUnselectStyle != null
                    ? widget.tabUnselectStyle
                    : TextStyle(
                        fontSize: 12,
                        color: widget.type == BottomNavigationBarType.shifting
                            ? Colors.white
                            : Colors.grey),
          ),
          icon,
          activeIcon,
          widget.tabIconColors[currentIndex]);
      list.add(item);
    }

    return list;
  }

  ///BottomNavigationBarItem
  BottomNavigationBarItem _buildNavigationItem(
      Text title, Icon icon, Icon activeIcon, backgroundColor) {
    return new BottomNavigationBarItem(
        icon: icon,
        activeIcon: activeIcon,
        title: title,
        backgroundColor: backgroundColor);
  }
}
