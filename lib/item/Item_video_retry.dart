import 'package:flutter/material.dart';
import 'package:com.sxt.flutter/values/colors.dart';

class VideoRetry extends StatelessWidget {
  final Function onRetry;
  final bool isCompleted;

  const VideoRetry({Key key, this.onRetry, this.isCompleted = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      child: InkWell(
        onTap: () {
          if (onRetry != null) {
            onRetry();
          }
        },
        child: Container(
          width: isCompleted ? 170 : 0,
          height: isCompleted ? 70 : 0,
          padding: EdgeInsets.only(left: 30, right: 30, top: 20, bottom: 20),
          decoration: ShapeDecoration(
              color: ColorStyle.alpha,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4)))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'images/icon_video_refresh.png',
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: Text(
                  '重新播放',
                  style: TextStyle(color: ColorStyle.white, fontSize: 17),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
