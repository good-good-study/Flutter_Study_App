import 'package:flutter/material.dart';
import 'package:com.sxt.flutter/values/colors.dart';

class VideoDrawer extends StatefulWidget {
  final List<String> list;
  final OnDrawerClickListener onDrawerClickListener;

  const VideoDrawer({Key key, this.list, this.onDrawerClickListener})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _VideoDrawerState();
}

class _VideoDrawerState extends State<VideoDrawer> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.list.length + 1,
        itemBuilder: (context, index) {
          if (index == 0) {
            return _buildTitle();
          } else {
            return _buildList(index - 1, widget.list[index - 1]);
          }
        });
  }

  Widget _buildTitle() {
    return Stack(
      children: <Widget>[
        Align(
          alignment: AlignmentDirectional.centerStart,
          child: Text(
            '章节',
            style: TextStyle(color: ColorStyle.white, fontSize: 17),
          ),
        ),
        Align(
          alignment: AlignmentDirectional.centerEnd,
          child: InkWell(
            onTap: () {
              if (widget.onDrawerClickListener != null) {
                widget.onDrawerClickListener.onClosedClick();
              }
            },
//            child: Image.asset('images/ic_back_white.png'),
            child: Icon(
              Icons.arrow_back,
              color: ColorStyle.white,
              size: 24,
            ),
          ),
        )
      ],
    );
  }

  Widget _buildList(int index, String data) {
    return InkWell(
      onTap: () {
        if (widget.onDrawerClickListener != null) {
          widget.onDrawerClickListener.onItemClick(index);
        }
      },
      child: Padding(
        padding: EdgeInsets.only(left: 20, top: 15, right: 20),
        child: Row(
          children: <Widget>[
            Text(
              index < 10 ? '0$index' : '$index',
              style: TextStyle(color: ColorStyle.white, fontSize: 15),
            ),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: Text(
                data,
                style: TextStyle(color: ColorStyle.white, fontSize: 15),
              ),
            )
          ],
        ),
      ),
    );
  }
}

abstract class OnDrawerClickListener {
  void onClosedClick();

  void onItemClick(int position);
}
