import 'package:flutter/material.dart';
import 'package:com.sxt.flutter/item/loading_animation.dart';

class Loading {
  LoadingDialog _loading;

  void showLoading(BuildContext context, bool isCancelable) {
    _loading = new LoadingDialog(
      _buildImages(),
      70,
      70,
      500,
    );
    showDialog(
        barrierDismissible: isCancelable,
        context: context,
        builder: (context) {
          return _loading;
        });
  }

  void dismiss() {
    if (_loading != null) {
      _loading.dismiss();
    }
  }
}

///动画所需的资源图片
List<String> _buildImages() {
  List<String> list = new List();
  list.add('assets/images/loading1.png');
  list.add('assets/images/loading2.png');
  list.add('assets/images/loading3.png');
  list.add('assets/images/loading4.png');

  return list;
}
