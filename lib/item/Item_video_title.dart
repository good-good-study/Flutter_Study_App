import 'package:com.sxt.flutter/values/colors.dart';
import 'package:flutter/material.dart';

class VideoTitle extends StatelessWidget {
  final String title;
  final bool isShowRight;
  final Function onBackClick, onRightClick;

  const VideoTitle(
      {Key key,
      this.title,
      this.isShowRight = false,
      this.onBackClick,
      this.onRightClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: AlignmentDirectional.topCenter,
      child: Container(
        height: (60 + MediaQuery.of(context).padding.top).toDouble(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [ColorStyle.black, ColorStyle.transparent],
              begin: AlignmentDirectional.topCenter,
              end: AlignmentDirectional.bottomCenter),
        ),
        padding: EdgeInsets.only(
            left: 15,
            right: 15,
            top: MediaQuery.of(context).padding.top,
            bottom: 15),
        child: Stack(
          children: <Widget>[
            Align(
              alignment: AlignmentDirectional.topStart,
              child: InkWell(
                onTap: () {
                  if (onBackClick != null) {
                    onBackClick();
                  }
                },
                child: Image.asset(
                  'assets/images/ic_back_white.png',
                  width: isShowRight ? 24 : 0,
                  height: isShowRight ? 24 : 0,
                ),
              ),
            ),
            Align(
              alignment: AlignmentDirectional.topCenter,
              child: Text(
                '长期照护理论',
                style: TextStyle(color: ColorStyle.white, fontSize: 18),
              ),
            ),
            Align(
                alignment: AlignmentDirectional.topEnd,
                child: InkWell(
                  child: Text(
                    isShowRight ? '章节' : '',
                    style: TextStyle(color: ColorStyle.white, fontSize: 18),
                  ),
                  onTap: () {
                    if (onRightClick != null) {
                      onRightClick();
                    }
                  },
                ))
          ],
        ),
      ),
    );
  }
}
