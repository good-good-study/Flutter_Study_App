import 'package:flutter/material.dart';
import 'package:com.sxt.flutter/values/colors.dart';

class CourseItem extends StatelessWidget {
  final String title;
  final String icon_path;
  final Function onItemClick;

  const CourseItem({Key key, this.title, this.icon_path, this.onItemClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20, left: 20, right: 20),
      padding: EdgeInsets.only(left: 20, right: 20, top: 30, bottom: 30),
      decoration: ShapeDecoration(
          color: ColorStyle.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)))),
      child: InkWell(
        splashColor: ColorStyle.alpha,
        onTap: () {
          if (onItemClick != null) {
            onItemClick();
          }
        },
        child: Row(
          children: <Widget>[
            Container(
              alignment: AlignmentDirectional.centerStart,
              margin: EdgeInsets.only(right: 10),
              child: Image.asset(
                icon_path,
                width: 50,
                height: 52,
              ),
            ),
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                      color: ColorStyle.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8),
                  child: Text(
                    title,
                    style: TextStyle(
                      color: ColorStyle.main_pink,
                      fontSize: 14,
                    ),
                  ),
                )
              ],
            )),
            Image.asset('images/common_icon_more.png')
          ],
        ),
      ),
    );
  }
}
