import 'package:com.sxt.flutter/values/TextStyles.dart';
import 'package:flutter/material.dart';

class VideoCourseTitle extends StatelessWidget {
  final bool isPortrait;
  final String title;

  const VideoCourseTitle({Key key, this.isPortrait, @required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: 5,
          height: 15,
          color: Colors.deepPurple,
        ),
        Container(
          margin: EdgeInsets.only(left: 10),
          child: Text(
            '章节',
            style: TextStyles.text_color_333_17_bold_style,
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 20),
          child: Text(
            title,
            style: TextStyles.text_color_333_17_bold_style,
          ),
        )
      ],
    );
  }
}
