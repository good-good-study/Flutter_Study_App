import 'package:flutter/material.dart';
import 'package:com.sxt.flutter/values/colors.dart';
import 'package:com.sxt.flutter/values/strings.dart';

///工单无数据界面
class WidgetNoData extends StatelessWidget {
  final String message;

  final String iconPath;

  WidgetNoData(
      {this.message = Strings.no_data,
      this.iconPath = 'assets/images/ic_no_data.png'});

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: double.infinity,
      height: double.infinity,
      color: ColorStyle.white,
      alignment: AlignmentDirectional.topCenter,
      child: Column(
        children: <Widget>[
          Padding(
            child: new Image.asset(iconPath != null ? iconPath : ''),
            padding: EdgeInsets.only(top: 50),
          ),
          Padding(
            child: Text(
              message,
              style: TextStyle(color: Colors.black45, fontSize: 15),
            ),
            padding: EdgeInsets.only(top: 16),
          ),
        ],
      ),
    );
  }
}
