import 'package:flutter/material.dart';
import 'package:com.sxt.flutter/values/colors.dart';

/**
 * 视频播放的 drawer的content
 */
class VideoDrawerContent extends StatelessWidget {
  final int index;
  final String data;
  final Function(int position) onDrawerItemClick;

  const VideoDrawerContent(
      {Key key, this.index, this.data, this.onDrawerItemClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    int index = this.index + 1;
    return InkWell(
      onTap: () {
        if (onDrawerItemClick != null) {
          onDrawerItemClick(this.index);
        }
      },
      child: Padding(
        padding: EdgeInsets.only(left: 20, top: 14, right: 20, bottom: 14),
        child: Row(
          children: <Widget>[
            Text(
              index < 10 ? '0$index' : '$index',
              style: TextStyle(color: ColorStyle.white, fontSize: 15),
            ),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: Text(
                data,
                style: TextStyle(color: ColorStyle.white, fontSize: 15),
              ),
            )
          ],
        ),
      ),
    );
  }
}
