import 'package:flutter/material.dart';
import 'package:com.sxt.flutter/values/colors.dart';

// ignore: must_be_immutable
class LoadingDialog extends Dialog {
  final List<String> _imgList;
  final double width;
  final double height;
  int interval = 600;
  BuildContext _context;

  LoadingDialog(this._imgList, this.width, this.height, this.interval);

  @override
  Widget build(BuildContext context) {
    _context = context;
    return _LoadingAnimation(
      _imgList,
      width: width,
      height: height,
      interval: interval,
    );
  }

  void dismiss() {
    Navigator.pop(_context);
  }
}

/// 帧动画Image
// ignore: must_be_immutable
class _LoadingAnimation extends StatefulWidget {
  final List<String> _imgList;
  final double width;
  final double height;
  int interval = 600;

  _LoadingAnimation(this._imgList, {this.width, this.height, this.interval});

  @override
  State<StatefulWidget> createState() => _LoadingState();
}

class _LoadingState extends State<_LoadingAnimation>
    with SingleTickerProviderStateMixin {
  // 动画控制
  Animation<double> _animation;
  AnimationController _controller;
  int interval = 600;

  @override
  void initState() {
    super.initState();

    if (widget.interval != null) {
      interval = widget.interval;
    }
    final int imageCount = widget._imgList.length;
    final int maxTime = interval * imageCount;

    // 启动动画controller
    _controller = new AnimationController(
        duration: Duration(milliseconds: maxTime), vsync: this);
    _controller.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        _controller.forward(from: 0.0); // 完成后重新开始
      }
    });

    _animation = new Tween<double>(begin: 0, end: imageCount.toDouble())
        .animate(_controller)
          ..addListener(() {
            setState(() {});
          });

    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ///通过不断更新UI,切换不同的图片
    int index = _animation.value.floor() % widget._imgList.length;
    return new Stack(
      children: <Widget>[
        Align(
          alignment: AlignmentDirectional.center,
          child: new Container(
              width: widget.width,
              height: widget.height,
              alignment: AlignmentDirectional.center,
              decoration: new ShapeDecoration(
                  color: ColorStyle.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)))),
              child: Image.asset(
                widget._imgList[index],
              )),
        ),
      ],
    );
  }
}
