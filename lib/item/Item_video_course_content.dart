import 'package:flutter/material.dart';
import 'package:com.sxt.flutter/values/colors.dart';

///视频播放界面的章节列表
class VideoCourseContent extends StatelessWidget {
  final int index;
  final String title;
  final String status;
  final Function(int index) onItemClick;

  const VideoCourseContent(
      {Key key, this.index, this.title, this.status, this.onItemClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    int index = this.index + 1;
    return Container(
      margin: EdgeInsets.only(top: 20, left: 15, right: 15),
      child: Row(
        children: <Widget>[
          Container(
            alignment: AlignmentDirectional.center,
            width: 20,
            height: 20,
            child: Text(
              index >= 10 ? '$index' : '0$index',
              style: TextStyle(color: ColorStyle.white, fontSize: 12),
            ),
            decoration: ShapeDecoration(
                color: status == '正在播放'
                    ? ColorStyle.main_pink
                    : ColorStyle.gray_d9d9d9,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)))),
          ),
          Expanded(
              child: Padding(
            padding: EdgeInsets.only(left: 10),
            child: InkWell(
              onTap: () {
                if (onItemClick != null) onItemClick(this.index);
              },
              child: Container(
                padding:
                    EdgeInsets.only(left: 22, right: 15, top: 20, bottom: 20),
                decoration: ShapeDecoration(
                    color: ColorStyle.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)))),
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: AlignmentDirectional.centerStart,
                      child: Text(
                        title,
                        style: TextStyle(
                            fontSize: 15,
                            color: status == null
                                ? ColorStyle.black
                                : (status == '已看'
                                    ? ColorStyle.gray_d9d9d9
                                    : ColorStyle.main_pink)),
                      ),
                    ),
                    Align(
                      alignment: AlignmentDirectional.centerEnd,
                      child: Text(
                        status == null ? '' : status,
                        style: TextStyle(
                            fontSize: 14,
                            color: status == '已看'
                                ? ColorStyle.gray_d9d9d9
                                : ColorStyle.gray_e5e5e5),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ))
        ],
      ),
    );
  }
}
