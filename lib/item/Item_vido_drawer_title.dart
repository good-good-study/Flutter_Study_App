import 'package:flutter/material.dart';
import 'package:com.sxt.flutter/values/colors.dart';

/**
 * 视频播放的 drawer的title
 */
class VideoDrawerTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.38,
      padding: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
      color: ColorStyle.black,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: AlignmentDirectional.centerStart,
            child: Text(
              '章节',
              style: TextStyle(color: ColorStyle.white, fontSize: 17),
            ),
          ),
          Align(
            alignment: AlignmentDirectional.centerEnd,
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
//              child: Image.asset('images/ic_back_white.png'),
              child: Icon(
                Icons.close,
                color: ColorStyle.white,
                size: 24,
              ),
            ),
          )
        ],
      ),
    );
  }
}
