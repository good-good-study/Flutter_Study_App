import 'dart:ui';

///16进制颜色值
///Widget颜色值不能使用非0xFF的值 , 想要使用透明度,可以使用Opcity组件
class ColorStyle {
  static const Color black = Color(0xFF000000);
  static const Color white = Color(0xFFFFFFFF);
  static const Color main_blue = Color(0xFF35578B);
  static const Color transparent = Color(0x00000000);
  static const Color alpha = Color(0x80000000); //50%透明度
  static const Color alpha_76 = Color(0x3D000000); //76%
  static const Color main_yellow = Color(0xFFED934D);
  static const Color main_yellow_dark = Color(0xFFCD6614);
  static const Color main_pink = Color(0xFFFFA1A1);
  static const Color gray_efeff4 = Color(0xFFEFEFF4);
  static const Color gray_e5e5e5 = Color(0xFFE5E5E5);
  static const Color gray_d9d9d9 = Color(0xFFD9D9D9);
  static const Color gray_889397 = Color(0xFF889397);

  static const Color text_color_333 = Color(0xFF333333);
  static const Color text_color_666 = Color(0xFF666666);
  static const Color text_color_999 = Color(0xFF999999);
}
