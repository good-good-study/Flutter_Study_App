class Strings {
  static const request_data_error = '数据请求失败,请稍后重试';
  static const String no_data = '还啥都没有哟~';
  static const String no_more_data = '没有更多数据了~';
  static const history = '历史';

  static const title_order_history = '工单历史';
}
