import 'package:flutter/material.dart';
import 'package:com.sxt.flutter/values/colors.dart';

class TextStyles {
  static const TextStyle text_color_white_12_style =
      TextStyle(color: ColorStyle.white, fontSize: 12);
  static const TextStyle text_color_white_14_style =
      TextStyle(color: ColorStyle.white, fontSize: 14);
  static const TextStyle text_color_white_15_style =
      TextStyle(color: ColorStyle.white, fontSize: 15);
  static const TextStyle text_color_white_17_style =
      TextStyle(color: ColorStyle.white, fontSize: 17);
  static const TextStyle text_color_white_18_style =
      TextStyle(color: ColorStyle.white, fontSize: 18);

  static const TextStyle text_color_333_12_style =
      TextStyle(color: ColorStyle.text_color_333, fontSize: 12);

  static const TextStyle text_color_333_14_style =
      TextStyle(color: ColorStyle.text_color_333, fontSize: 14);

  static const TextStyle text_color_333_15_style =
      TextStyle(color: ColorStyle.text_color_333, fontSize: 15);

  static const TextStyle text_color_333_17_style =
      TextStyle(color: ColorStyle.text_color_333, fontSize: 17);

  static const TextStyle text_color_333_17_bold_style = TextStyle(
      color: ColorStyle.text_color_333,
      fontSize: 17,
      fontWeight: FontWeight.bold);
}
