import 'package:com.sxt.flutter/pages/ChartApp.dart';
import 'package:com.sxt.flutter/pages/Test.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('测试', () {
    testWidgets('测试Chart', (WidgetTester tester) async {
      await tester.pumpWidget(ChartApp()); //构建widget
//      tester.pump(Duration(seconds: 2));//在给定的持续时间后触发Widget的重建。
      tester.pumpAndSettle();
    });
    testWidgets('测试 查找widget 中的文字', (WidgetTester tester) async {
      await tester.pumpWidget(Test());
      expect(find.text('hello'), findsWidgets);
    });
  });
}
