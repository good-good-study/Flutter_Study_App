import 'package:flutter_test/flutter_test.dart';

///测试某个函数
void main() {
  group('基础测试', () {
    test('检测字符串截取', () {
      var message = '文本截取';
      expect(message.substring(0, 2), equals('文本'));
    });

    test('比较数字大小 -> 同步', () {
      var num = 100;
      expect(100, num);
    });

    test('比较数字大小 -> 异步', () async {
      var num = 100;
      expect(
          100,
          await Future.delayed(Duration(seconds: 2)).then((_) {
            return num;
          }));
    });
  });
}
